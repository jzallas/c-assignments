//File:         Node.h
//Author(s):    Jonathan Zallas
//Created on:   October 4, 2012
//Description:  Node for a tree
//Usage:        Each node in the tree stores words and the line numbers
//              that the word was found.
//Version:      1.0
//

#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

#include <iostream>
#include <string>
#include <list>
using namespace std;


class Node {
private:
	string word;
	Node* left;
	Node* right;
	list<unsigned int> lines;
	list<unsigned int>::iterator it;
	bool exists;
public:
	Node();
	Node(string, unsigned int);
	string getWord()
	{
	    return word;
    };
	Node* getLeft()
	{
	    return left;
    };
	Node* getRight()
	{
	    return right;
    };
	void setLeft(Node*);
	void setRight(Node*);
	void setWord(string);
	void updateLineOccurrence(unsigned int);
	void printLineOccurrence();
	void setExists(bool DoesItExist);
    bool getExists();
};





#endif // NODE_H_INCLUDED
