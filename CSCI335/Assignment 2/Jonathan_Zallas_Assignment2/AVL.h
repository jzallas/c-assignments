//File:         AVL.h
//Author(s):    Jonathan Zallas
//Created on:   October 4, 2012
//Description:  AVL Tree
//Usage:        Binary Search Tree that balances itself upon inserting
//              Uses lazy deletion to delete
//Version:      1.0
//

#ifndef AVL_H_INCLUDED
#define AVL_H_INCLUDED

#include "Node.h"
#include <iostream>
#include <string>
#include <cmath> // for log2()

using namespace std;

class AVLTree{
private:
	Node* root;
	Node* curNode;
	void wipeTree(Node*);
	void add(Node*, string, unsigned int);
	void preOrder(Node*);
	void postOrder(Node*);
	void inOrder(Node*);
	Node* findParent(Node*);

	//functions to balance tree
	int height(Node* node);
	int balanceFactor(Node* node);
	void balanceNode (Node* node);
	void balanceTree(Node*node);

	//Functions to find average
	int depth(Node* node);
	int totalDepth(Node* node);

	int size;
	int depthTotal;

public:
    int recursiveCounter;
	AVLTree();
	AVLTree(string, unsigned int);
	~AVLTree();

	float averageDepth();
	Node* seek(string);
	void insert(string, unsigned int);
	void remove(string);
	void preOrderTraversal();
	void postOrderTraversal();
	void inOrderTraversal();
	bool find(string);
	void lazyRemove(string);
	int treeSize()
	{
	    return size;
    };
};


#endif // AVL_H_INCLUDED
