Assignment 2
Jonathan Zallas (jzallas@hunter.cuny.edu)

--Implemented both trees.

--Both trees are fully functional. Both trees work with every *.txt file except for the ones provided by the professor (DocumentHW2.txt and wordsHW2.txt). When using these files, the program segfaults when attempting to erase every other word. This only happens for these text files. A temporary work around to produce a working program with these text files involves using lazy deletion in both the BinarySearchTree and the AVL Tree. This causes the average to report the same each time it is calculated because nodes aren't actually deleted. This also causes the recursive count to come out the same for some functions since the tree doesn't get any smaller.

--To compile just type

         make all

--To run the first program type
     
	 testTrees <document file name> <words file name> <Flag> 

--To remove object files type

         make clean
