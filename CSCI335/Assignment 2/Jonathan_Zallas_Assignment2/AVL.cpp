//File:         AVL.cpp
//Author(s):    Jonathan Zallas
//Created on:   October 4, 2012
//Description:  AVL Tree
//Usage:        Binary Search Tree that balances itself upon inserting
//              Uses lazy deletion to delete
//Version:      1.0
//

#include "AVL.h"

AVLTree::AVLTree(){
	root = NULL;
	size = 0;
	recursiveCounter = 0;
}


AVLTree::AVLTree(string word, unsigned int line){
	Node* temp = new Node(word, line);
	root = temp;
	size = 1;
}


AVLTree::~AVLTree(){
	wipeTree(root);
}


void AVLTree::insert(string word, unsigned int line){
	if (root == NULL){
		root = new Node(word, line);
		size++;
		recursiveCounter++;
	}
	else
        {
            Node* foundNode = seek(word);
            if (foundNode == NULL) //the word doesn't exist yet
            {
                add(root, word, line);

            }
            else if (foundNode->getWord() == word) //if the word already exists
            {
                if(!foundNode->getExists())
                    foundNode->setExists(true);
                foundNode->updateLineOccurrence(line);
                recursiveCounter++;
            }
        }
}


Node* AVLTree::seek(string word){
	curNode = root;
	while(curNode != NULL){
	    recursiveCounter++;
		if(curNode->getWord() == word){
			return curNode;
		}
		else {
			if(word > curNode->getWord())	curNode = curNode->getRight();
			else curNode = curNode->getLeft();
		}
	}
	recursiveCounter++;
	return NULL;
}


void AVLTree::remove(string word){
	if(root == NULL){
		cout << "No tree or it's empty!" << endl;
		return;
	}
		else if (root->getWord() == word){
		return;
	}
	bool found = false;
	Node* parent;
	curNode = root;
	while(curNode != NULL){
		if(curNode->getWord() == word){
			found = true;
			break;
		}
		else {
			parent = curNode;
			if(word > curNode->getWord())	curNode = curNode->getRight();
			else curNode = curNode->getLeft();
		}
	}
	if(!found){
		cout << "Not in tree." << endl;
		return;
	}

	if((curNode->getLeft() == NULL) && (curNode->getRight() == NULL))//LEAD
	{
		if(parent->getLeft() == curNode) parent->setLeft(NULL);
		else parent->setRight(NULL);

		delete curNode;
		size--;
		return;
	}


	if((curNode->getLeft() == NULL && curNode->getRight() != NULL) || (curNode->getLeft() != NULL && curNode->getRight() == NULL)) //ONE CHILD
	{

			if(curNode->getLeft() == NULL && curNode->getRight() != NULL) {

				if(parent->getLeft() == curNode){
					parent->setLeft(curNode->getRight());
				}
				else{
					parent->setRight(curNode->getRight());
				}
			}
			else {
				if(parent->getLeft() == curNode){
					parent->setLeft(curNode->getLeft());
				}
				else{
					parent->setRight(curNode->getLeft());
				}
			}

			delete curNode;
			size--;
			return;
	}

	if((curNode->getLeft() != NULL)&&(curNode->getRight() != NULL)) //TWO CHILDREN
	{
		Node* treeChecker;
		treeChecker = curNode->getRight();

		if((treeChecker->getLeft() == NULL) &&(treeChecker->getRight() == NULL)){
			curNode->setWord(treeChecker->getWord());
			delete treeChecker;
			size--;
			curNode->setRight(NULL);
		}
		else{
			if(curNode->getRight()->getLeft() != NULL){
				Node* leftCurrent;
				Node* leftCurrentParent;
				leftCurrentParent = curNode->getRight();
				leftCurrent = leftCurrentParent->getLeft();
				while(leftCurrent->getLeft() != NULL){
					leftCurrentParent = leftCurrent;
					leftCurrent = leftCurrent->getLeft();
				}
				curNode->setWord(leftCurrent->getWord());

				delete leftCurrent;
				size--;
				leftCurrentParent->setLeft(NULL);
			}
			else{
				Node* rightTemp;
				rightTemp = curNode->getRight();
				curNode->setWord(rightTemp->getWord());
				delete rightTemp;
				size--;
			}
		}
		return;
	}

}


void AVLTree::add(Node* root, string word, unsigned int line){
    recursiveCounter++;
	if (word < root->getWord()){
		if(root->getLeft() != NULL){
			add(root->getLeft(), word, line);
		}
		else{
			root->setLeft(new Node(word, line));
			balanceTree(root);
			size++;
		}
	}
	else{
		if(root->getRight() != NULL){
			add(root->getRight(), word, line);
		}
		else{
			root->setRight(new Node(word, line));
			balanceTree(root);
			size++;
		}
	}
}


void AVLTree::wipeTree(Node* root){
	if(root != NULL){
		wipeTree(root->getLeft());
		wipeTree(root->getRight());
		delete root;
		size--;
	}
}


void AVLTree::preOrderTraversal(){
	preOrder(root);
}


void AVLTree::postOrderTraversal(){
	postOrder(root);
}


void AVLTree::inOrderTraversal(){
	inOrder(root);
}


void AVLTree::preOrder(Node* root){
	if (root->getExists())
	cout << root->getWord() << " ";
	if (root->getLeft() != NULL) {preOrder(root->getLeft());}
	if (root->getRight() != NULL) {preOrder(root->getRight());}
}


void AVLTree::postOrder(Node* root){
	if (root->getLeft() != NULL) {postOrder(root->getLeft());}
	if (root->getRight() != NULL) {postOrder(root->getRight());}
	if (root->getExists())
	cout << root->getWord() << " ";
}


void AVLTree::inOrder(Node* root){
	if (root->getLeft() != NULL) {inOrder(root->getLeft());}
	if (root->getExists())
	cout << root->getWord() << " ";
	if (root->getRight() != NULL) {inOrder(root->getRight());}
}


int AVLTree::depth(Node* node){
    int depth = 0;
    Node * temp = root;
    while (temp!= node)
    {
        depth++;
        if (node->getWord() < temp->getWord())
            temp = temp->getLeft();
        else
            temp = temp->getRight();
    }
    return depth;
}


int AVLTree::totalDepth(Node* node){
    if(treeSize() != 0)
    {
        if (node->getLeft() != NULL)
            totalDepth(node->getLeft());
        depthTotal = depthTotal + depth(node);
        if (node->getRight() !=NULL)
            totalDepth(node->getRight());
    }
    return depthTotal;
}


float AVLTree::averageDepth(){
    depthTotal = 0;
    float average = ((float(totalDepth(root)))/treeSize())/log2(treeSize());
    depthTotal = 0;
    return(average);
}


int AVLTree::height(Node* node){
    int leftTree = 0;
    int rightTree = 0;
    if (node == NULL)
        return 0;
    if (node->getLeft() != NULL)
    {
        leftTree = height(node->getLeft());
        leftTree++;
    }
    if (node->getRight() != NULL)
    {
        rightTree = height(node->getRight());
        rightTree++;
    }

    if (leftTree > rightTree)
    {
        return leftTree;
    }
    else
    {
        return rightTree;
    }
}


int AVLTree::balanceFactor(Node* node){
    int left, right;
    left = height(node->getLeft());
    right = height(node->getRight());
    if (left > 0 || node->getLeft() != NULL)
        left++;
    if (right > 0 || node->getRight() != NULL)
        right++;

    return (left-right);
}


Node* AVLTree::findParent(Node* child){
    curNode = root;
    Node* parent = root;

	while(curNode != NULL){
		if(curNode== child){
			return parent;
		}
		else {
			if(child->getWord() > curNode->getWord())
			{
                parent = curNode;
                curNode = curNode->getRight();
			}
			else
			{
			    parent = curNode;
                curNode = curNode->getLeft();
			}
		}
	}
	return NULL;

}


void AVLTree::balanceNode(Node* node){
    int balance = balanceFactor(node);

    //right-right and right-left case
    if (balance == -2)
    {
        balance = balanceFactor(node->getRight()); //balance factor of right child must be checked
        if (balance == 1)//right left - single right
        {
            Node* child = node->getRight();
            Node* temp = child->getLeft()->getRight();
            node->setRight(child->getLeft());
            node->getRight()->setRight(child);
            child->setLeft(temp);
        }
        if (balance == -1 || balance == 1)//right right - single left rotation
        {
            if (node == root)
            {
                Node* temp = node->getRight()->getLeft();
                node->getRight()->setLeft(root);
                root = node->getRight();
                root->getLeft()->setRight(temp);
            }
            else
            {
                Node* parent = findParent(node);
                if (parent->getLeft() == node)
                {
                    Node* temp = node->getRight()->getLeft();
                    node->getRight()->setLeft(node);
                    parent->setLeft(node->getRight());
                    node->setRight(temp);

                }
                else if (parent->getRight() == node)
                {
                    Node* temp = node->getRight()->getLeft();
                    node->getRight()->setLeft(node);
                    parent->setRight(node->getRight());
                    node->setRight(temp);
                }
            }
        }
    }

    //left-left and left-right case
    else if (balance == 2)
    {
        balance = balanceFactor(node->getLeft()); //balance factor of left child must be checked
        if (balance == -1)//left right - single left
        {
            Node* child = node->getLeft();
            Node* temp = child->getRight()->getLeft();
            node->setLeft(child->getRight());
            node->getLeft()->setLeft(child);
            child->setRight(temp);
        }
        else if (balance == 1 || balance == -1)//left left - single right rotation
        {
            if (node == root)
            {
                Node* temp = node->getLeft()->getRight();
                node->getLeft()->setRight(root);
                root = node->getLeft();
                root->getRight()->setLeft(temp);
            }
            else
            {
                Node* parent = findParent(node);
                if (parent->getLeft() == node)
                {
                    Node* temp = node->getLeft()->getRight();
                    node->getLeft()->setRight(node);
                    parent->setLeft(node->getLeft());
                    node->setLeft(temp);
                }
                else if (parent->getRight() == node)
                {
                    Node* temp = node->getLeft()->getRight();
                    node->getLeft()->setRight(node);
                    parent->setRight(node->getLeft());
                    node->setLeft(temp);
                }
            }
        }
    }
}


void AVLTree::balanceTree(Node*node){
        balanceNode(node);
        if (node !=root)
            balanceTree(findParent(node));
}


bool AVLTree::find(string word){
    if (seek(word)!= NULL)
        return true;
    return false;
}


void AVLTree::lazyRemove(string word){
    Node* temp = seek(word);
    if (temp != NULL)
        temp->setExists(false);
}
