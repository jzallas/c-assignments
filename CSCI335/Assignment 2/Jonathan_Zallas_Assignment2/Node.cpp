//File:         Node.cpp
//Author(s):    Jonathan Zallas
//Created on:   October 4, 2012
//Description:  Node for a tree
//Usage:        Each node in the tree stores words and the line numbers
//              that the word was found.
//Version:      1.0
//

#include "Node.h"

Node::Node(){
	word = "";
	left = right = NULL;
	exists = true;
}

Node::Node(string newWord, unsigned int line){
	word = newWord;
	left = right = NULL;
	updateLineOccurrence(line);
	exists = true;
}

void Node::setLeft(Node* node){
	left = node;
}

void Node::setRight(Node* node){
	right = node;
}

void Node::setWord(string newWord){
	word = newWord;
}

void Node::updateLineOccurrence(unsigned int line){
    bool alreadyExists = false;
    //check to see if the line that this word appears on is already documented
    for (it=lines.begin(); it != lines.end(); it++ )
    {
        if (*it == line)
            alreadyExists = true;
    }
    //if the line that this word appears on was not previously recorded
    if (!alreadyExists)
    {
        lines.push_back(line); //add the line
        lines.sort(); //sort the appeared lines from smallest to largest
    }
}

void Node::printLineOccurrence(){
    for (it=lines.begin(); it != lines.end(); it++)
        cout << " " << *it;
}

void Node::setExists(bool DoesItExist){
    if (!DoesItExist)
        lines.clear();
    exists = DoesItExist;
}

bool Node::getExists(){
    return exists;
}
