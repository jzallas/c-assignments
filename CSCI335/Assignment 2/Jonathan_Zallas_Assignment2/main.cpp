//File:         main.cpp
//Author(s):    Jonathan Zallas
//Created on:   October 4, 2012
//Description:  Tests BST and AVL
//Usage:        Runs in terminal using <programname> <file1> <file2> <flag>
//Version:      1.0
//

#include <iostream>
#include <string>
#include "BST.h"
#include "Node.h"
#include "AVL.h"
#include <sstream>
#include <fstream>
#include <cstdlib>

using namespace std;

string checkIfValid(string currentWord)
{
    for(unsigned int i = 0; i < currentWord.size() ; i++)
    {
        currentWord[i] = tolower(currentWord[i]);
        if (!isalpha((char)currentWord.c_str()[i]))
        {
            if (currentWord.compare(i, 1, "\n"))
            {
                currentWord.erase(i,1);
                i--;
            }
        }
    }
    return (currentWord);
};

void testBST(char* file1, char* file2)
{
    BinarySearchTree* tree = new BinarySearchTree;
    ifstream DOCFILE(file1);
    ifstream WORDFILE(file2);



    //A)LOAD FILE INTO TREE
    int lineCounter = 0; // keeps track of which line is being streamed
    string line;
    while(DOCFILE) // while stream has data to read
    {
        getline(DOCFILE, line); // get 1 line, append it to 'word'
        lineCounter++;
        stringstream ss; // stream for getline
        ss << line;

        while(ss) // while stream has data to read
        {
            string word;
            ss >> word; // get one word
            if(ss) // without this if statement, the last word will be read twice
            {

                if(!word.empty())
                {

                    tree->insert(checkIfValid(word), lineCounter);
                }
            }
        }
    }
    DOCFILE.close();

    cout << "Insert had " << tree->recursiveCounter << " calls." << endl;
    tree->recursiveCounter = 0;



    //B)AVERAGE DEPTH
    cout << "Average Depth is " << tree->averageDepth() << endl;

    //C)FIND EVERY WORD
    while(WORDFILE) // while stream has data to read
    {
        getline(WORDFILE, line); // get 1 line, append it to 'word'
        lineCounter++;
        stringstream ss; // stream for getline
        ss << line;

        while(ss) // while stream has data to read
        {
            string word;
            ss >> word; // get one word
            if(ss) // without this if statement, the last word will be read twice
            {

                if(!word.empty())
                {

                    tree->find(checkIfValid(word));
                }
            }
        }
    }
    WORDFILE.close();

    cout << "Searching for each word in " << file2 << " had " << tree->recursiveCounter << " calls." << endl;
    tree->recursiveCounter = 0;

    //D)SEARCH FOR A SPECIFIC WORD
    cout << "Please enter only ONE word: ";
    cin >> line;
    line = checkIfValid(line);
    if (tree->find(line))
    {
        cout << tree->seek(line)->getWord() << " appears on lines";
        tree->seek(line)->printLineOccurrence();
        cout << endl;
    }
    else
        cout << line << " is not in the tree." << endl;
    tree->recursiveCounter = 0;

    //E)ERASE EVERY OTHER WORD
    bool skip = false;
    WORDFILE.open(file2);
    while(WORDFILE) // while stream has data to read
    {
        getline(WORDFILE, line); // get 1 line, append it to 'word'
        stringstream ss; // stream for getline
        ss << line;

        while(ss) // while stream has data to read
        {
            string word;
            ss >> word; // get one word
            if(ss) // without this if statement, the last word will be read twice
            {

                if(!word.empty())
                {
                    if(skip == false)
                    {
                        tree->lazyRemove(checkIfValid(word));
                        skip = true;
                    }
                    else
                        skip = false;
                }
            }
        }
    }
    WORDFILE.close();

    cout << "Deleting every other word in " << file2 << " had " << tree->recursiveCounter << " calls." << endl;
    tree->recursiveCounter = 0;

    //B)AVERAGE DEPTH
    cout << "Average Depth is " << tree->averageDepth() << endl;

    //C)FIND EVERY WORD
    WORDFILE.open(file2);
    while(WORDFILE) // while stream has data to read
    {
        getline(WORDFILE, line); // get 1 line, append it to 'word'
        lineCounter++;
        stringstream ss; // stream for getline
        ss << line;

        while(ss) // while stream has data to read
        {
            string word;
            ss >> word; // get one word
            if(ss) // without this if statement, the last word will be read twice
            {

                if(!word.empty())
                {

                    tree->find(checkIfValid(word));
                }
            }
        }
    }
    WORDFILE.close();

    cout << "Searching for each word in " << file2 << " had " << tree->recursiveCounter << " calls." << endl;
    tree->recursiveCounter = 0;

    delete tree;
};

void testAVL(char* file1, char* file2)
{
    AVLTree* tree = new AVLTree;
    ifstream DOCFILE(file1);
    ifstream WORDFILE(file2);



    //A)LOAD FILE INTO TREE
    int lineCounter = 0; // keeps track of which line is being streamed
    string line;
    while(DOCFILE) // while stream has data to read
    {
        getline(DOCFILE, line); // get 1 line, append it to 'word'
        lineCounter++;
        stringstream ss; // stream for getline
        ss << line;

        while(ss) // while stream has data to read
        {
            string word;
            ss >> word; // get one word
            if(ss) // without this if statement, the last word will be read twice
            {

                if(!word.empty())
                {

                    tree->insert(checkIfValid(word), lineCounter);
                }
            }
        }
    }
    DOCFILE.close();

    cout << "Insert had " << tree->recursiveCounter << " calls." << endl;
    tree->recursiveCounter = 0;



    //B)AVERAGE DEPTH
    cout << "Average Depth is " << tree->averageDepth() << endl;

    //C)FIND EVERY WORD
    while(WORDFILE) // while stream has data to read
    {
        getline(WORDFILE, line); // get 1 line, append it to 'word'
        lineCounter++;
        stringstream ss; // stream for getline
        ss << line;

        while(ss) // while stream has data to read
        {
            string word;
            ss >> word; // get one word
            if(ss) // without this if statement, the last word will be read twice
            {

                if(!word.empty())
                {

                    tree->find(checkIfValid(word));
                }
            }
        }
    }
    WORDFILE.close();

    cout << "Searching for each word in " << file2 << " had " << tree->recursiveCounter << " calls." << endl;
    tree->recursiveCounter = 0;

    //D)SEARCH FOR A SPECIFIC WORD
    cout << "Please enter only ONE word: ";
    cin >> line;
    line = checkIfValid(line);
    if (tree->find(line))
    {
        cout << tree->seek(line)->getWord() << " appears on lines";
        tree->seek(line)->printLineOccurrence();
        cout << endl;
    }
    else
        cout << line << " is not in the tree." << endl;
    tree->recursiveCounter = 0;

    //E)ERASE EVERY OTHER WORD
    bool skip = false;
    WORDFILE.open(file2);
    while(WORDFILE) // while stream has data to read
    {
        getline(WORDFILE, line); // get 1 line, append it to 'word'
        stringstream ss; // stream for getline
        ss << line;

        while(ss) // while stream has data to read
        {
            string word;
            ss >> word; // get one word
            if(ss) // without this if statement, the last word will be read twice
            {

                if(!word.empty())
                {
                    if(skip == false)
                    {
                        tree->lazyRemove(checkIfValid(word));
                        skip = true;
                    }
                    else
                        skip = false;
                }
            }
        }
    }
    WORDFILE.close();

    cout << "Deleting every other word in " << file2 << " had " << tree->recursiveCounter << " calls." << endl;
    tree->recursiveCounter = 0;

    //B)AVERAGE DEPTH
    cout << "Average Depth is " << tree->averageDepth() << endl;

    //C)FIND EVERY WORD
    WORDFILE.open(file2);
    while(WORDFILE) // while stream has data to read
    {
        getline(WORDFILE, line); // get 1 line, append it to 'word'
        lineCounter++;
        stringstream ss; // stream for getline
        ss << line;

        while(ss) // while stream has data to read
        {
            string word;
            ss >> word; // get one word
            if(ss) // without this if statement, the last word will be read twice
            {

                if(!word.empty())
                {

                    tree->find(checkIfValid(word));
                }
            }
        }
    }
    WORDFILE.close();

    cout << "Searching for each word in " << file2 << " had " << tree->recursiveCounter << " calls." << endl;
    tree->recursiveCounter = 0;

    delete tree;
};

int main(int argc, char **argv)
{
    //Check input arguments
    if (argc!=4)
    {
        cout << "Usage: " << argv[0] << " <document file> <words file> <flag>" << endl;
        cout << "     if <flag> is 0: using BinarySearchTree" << endl;
        cout << "     if <flag> is 1: using AVLTree" << endl;
        return 0;
    }
    int flag=atoi(argv[3]);

    if (flag == 1)
        testAVL(argv[1],argv[2]);
    else
        testBST(argv[1],argv[2]);


    return 0;
}

