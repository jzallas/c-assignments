//File:         BST.h
//Author(s):    Jonathan Zallas
//Created on:   October 4, 2012
//Description:  Binary Search Tree
//Usage:        Binary Search Tree
//              Uses lazy deletion to delete
//Version:      1.0
//

#ifndef BST_H_INCLUDED
#define BST_H_INCLUDED

#include "Node.h"
#include <iostream>
#include <string>
#include <cmath> // for log2()

using namespace std;

class BinarySearchTree{
private:
	Node* root;
	Node* curNode;
	void wipeTree(Node*);
	void add(Node*, string, unsigned int);
	void preOrder(Node*);
	void postOrder(Node*);
	void inOrder(Node*);

	//Functions to find average
	int depth(Node* node);
	int totalDepth(Node* node);

	int size;
	int depthTotal;

public:
    int recursiveCounter;
	BinarySearchTree();
	BinarySearchTree(string, unsigned int);
	~BinarySearchTree();

	bool find(string);
	float averageDepth();
	Node* seek(string);
	void insert(string, unsigned int);
	void remove(string);
	void preOrderTraversal();
	void postOrderTraversal();
	void inOrderTraversal();
	void lazyRemove(string);
	int treeSize()
	{
	    return size;
    };
};



#endif // BST_H_INCLUDED
