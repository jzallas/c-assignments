//File:         BST.cpp
//Author(s):    Jonathan Zallas
//Created on:   October 4, 2012
//Description:  Binary Search Tree
//Usage:        Binary Search Tree
//              Uses lazy deletion to delete
//Version:      1.0
//

#include "BST.h"

BinarySearchTree::BinarySearchTree(){
	root = NULL;
	size = 0;
	recursiveCounter = 0;
}


BinarySearchTree::BinarySearchTree(string word, unsigned int line){
	Node* temp = new Node(word, line);
	root = temp;
	size = 1;
}


BinarySearchTree::~BinarySearchTree(){
	wipeTree(root);
}


void BinarySearchTree::insert(string word, unsigned int line){
	if (root == NULL){
		root = new Node(word, line);
		size++;
		recursiveCounter++;
	}
	else
        {
            Node* foundNode = seek(word);
            if (foundNode == NULL) //the word doesn't exist yet
            {
                add(root, word, line);

            }
            else if (foundNode->getWord() == word) //if the word already exists
            {
                if(!foundNode->getExists())
                    foundNode->setExists(true);
                foundNode->updateLineOccurrence(line);
                recursiveCounter++;
            }
        }
}


Node* BinarySearchTree::seek(string word){
	curNode = root;
	while(curNode != NULL){
	    recursiveCounter++;
		if(curNode->getWord() == word){
			return curNode;
		}
		else {
			if(word > curNode->getWord())
                curNode = curNode->getRight();
			else
                curNode = curNode->getLeft();
		}
	}
	recursiveCounter++;
	return NULL;
}


void BinarySearchTree::remove(string word){
	if(root == NULL){
		cout << "No tree or it's empty!" << endl;
		return;
	}
		else if (root->getWord() == word){
		return;
	}
	bool found = false;
	Node* parent;
	curNode = root;

	while(curNode != NULL){
		if(curNode->getWord() == word){
			found = true;
			break;
		}
		else {
			parent = curNode;
			if(word > curNode->getWord())	curNode = curNode->getRight();
			else curNode = curNode->getLeft();
		}
	}
	if(!found){
		cout << "Not in tree." << endl;
		return;
	}

	if((curNode->getLeft() == NULL) && (curNode->getRight() == NULL)) // LEAF
	{
		if(parent->getLeft() == curNode) parent->setLeft(NULL);
		else parent->setRight(NULL);

		delete curNode;
		size--;
		return;
	}

	if((curNode->getLeft() == NULL && curNode->getRight() != NULL) || (curNode->getLeft() != NULL && curNode->getRight() == NULL)) //ONE CHILD
	{
			if(curNode->getLeft() == NULL && curNode->getRight() != NULL) {
				if(parent->getLeft() == curNode){
					parent->setLeft(curNode->getRight());
				}
				else{
					parent->setRight(curNode->getRight());
				}
			}
			else {
				if(parent->getLeft() == curNode){
					parent->setLeft(curNode->getLeft());
				}
				else{
					parent->setRight(curNode->getLeft());
				}
			}
			delete curNode;
			size--;
			return;
	}
	if((curNode->getLeft() != NULL)&&(curNode->getRight() != NULL)) // TWO CHILDREN
	{
		Node* temp;
		temp = curNode->getRight();

		if((temp->getLeft() == NULL) &&(temp->getRight() == NULL)){
			curNode->setWord(temp->getWord());
			delete temp;
			size--;
			curNode->setRight(NULL);
		}
		else{
			if(curNode->getRight()->getLeft() != NULL){
				Node* leftCurrent;
				Node* leftCurrentParent;
				leftCurrentParent = curNode->getRight();
				leftCurrent = leftCurrentParent->getLeft();
				while(leftCurrent->getLeft() != NULL){
					leftCurrentParent = leftCurrent;
					leftCurrent = leftCurrent->getLeft();
				}
				curNode->setWord(leftCurrent->getWord());

				delete leftCurrent;
				size--;
				leftCurrentParent->setLeft(NULL);
			}
			else{
				Node* rightTemp;
				rightTemp = curNode->getRight();
				curNode->setWord(rightTemp->getWord());
				delete rightTemp;
				size--;
			}
		}
		return;
	}

}


void BinarySearchTree::add(Node* root, string word, unsigned int line){
    recursiveCounter++;
	if (word < root->getWord()){
		if(root->getLeft() != NULL){
			add(root->getLeft(), word, line);
		}
		else{
			root->setLeft(new Node(word, line));
			size++;
		}
	}
	else{
		if(root->getRight() != NULL){
			add(root->getRight(), word, line);
		}
		else{
			root->setRight(new Node(word, line));
			size++;
		}
	}
}


void BinarySearchTree::wipeTree(Node* root){
	if(root != NULL){
		wipeTree(root->getLeft());
		wipeTree(root->getRight());
		delete root;
		size--;
	}
}


void BinarySearchTree::preOrderTraversal(){
	preOrder(root);
}


void BinarySearchTree::postOrderTraversal(){
	postOrder(root);
}


void BinarySearchTree::inOrderTraversal(){
	inOrder(root);
}


void BinarySearchTree::preOrder(Node* root){
    if (root->getExists())
	cout << root->getWord() << " ";
	if (root->getLeft() != NULL){preOrder(root->getLeft());}
	if (root->getRight() != NULL) {preOrder(root->getRight());}
}


void BinarySearchTree::postOrder(Node* root){
	if (root->getLeft() != NULL) {postOrder(root->getLeft());}
	if (root->getRight() != NULL) {postOrder(root->getRight());}
	if (root->getExists())
	cout << root->getWord() << " ";
}


void BinarySearchTree::inOrder(Node* root){
	if (root->getLeft() != NULL){inOrder(root->getLeft());}
    if (root->getExists())
        cout << root->getWord() << " ";
	if (root->getRight() != NULL) {inOrder(root->getRight());}
}


int BinarySearchTree::depth(Node* node){
    int depth = 0;
    Node * temp = root;
    while (temp!= node)
    {
        depth++;
        if (node->getWord() < temp->getWord())
            temp = temp->getLeft();
        else
            temp = temp->getRight();
    }
    return depth;
}


int BinarySearchTree::totalDepth(Node* node){
    if(treeSize() != 0)
    {
        if (node->getLeft() != NULL)
            totalDepth(node->getLeft());
        depthTotal = depthTotal + depth(node);
        if (node->getRight() !=NULL)
            totalDepth(node->getRight());
    }
    return depthTotal;
}


float BinarySearchTree::averageDepth(){
    depthTotal = 0;
    float average = ((float(totalDepth(root)))/treeSize())/log2(treeSize());
    depthTotal = 0;
    return(average);
}


bool BinarySearchTree::find(string word){
    if (seek(word)!= NULL){
        return true;
    }
    return false;
}


void BinarySearchTree::lazyRemove(string word){
    Node* temp = seek(word);
    if (temp != NULL)
        temp->setExists(false);
}

