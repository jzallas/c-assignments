//  main.cpp
//  Jonathan Zallas
//  Template chain class that creates an dynamic array of any type
//  09/10/12

#include <iostream>
#include "chain.h"
#include <cstdlib>

using namespace std;

int randInt(int a, int b) //random number between a and b (including a and b)
{
    return (rand() % (b-a+1) + a);
}

chain<int> algorithmone(int numbers)
{
//1. Fill the array A from A[0] to A[N-1] as follows: To fill A[i], generate random numbers
//(between 0 and N-1) until you get one that is not already in a[0], a[1], ..., a[i-1].
    chain<int> temp;
    int rnum = rand() % numbers;
    int sizeofarray = numbers;
    for (int n = 0; n < sizeofarray; n++) //do this as many times as u want the array to be
    {
        for (int k = 0; k < temp.size; k++)
        {
            if (temp.array[k] == rnum) //if the number is found
            {
                rnum = rand() % numbers; //generate new number
                k=-1;                // and start checking again from zero
            }
        }
        temp.append(rnum);
    }
    return temp;
}

chain<int> algorithmtwo(int numbers)
{
//2. Same as algorithm (1), but keep an extra array called the USED array. When a random
//number, ran, is first put in the array A, set USED[ran]=true (initially USED[] is false). Now
//you can easily check whether an item is already in an array (in constant time).
    chain<int> temp;
    bool USED[numbers];
    int rnum = rand() % numbers;
    int sizeofarray = numbers;
    for (int n = 0; n < sizeofarray; n++)
        USED[n] = false;

    for (int n = 0; n < sizeofarray; n++)
    {
        while (USED[rnum] == true)
        {
            rnum = rand() % numbers;
        }
        temp.append(rnum);
        USED[rnum] = true;
    }
    return temp;
}

chain<int> algorithmthree(int numbers)
{
//3. Fill the array such that a[i]=i+1. Then
//for (i=1; i<N; i++)
//swap( a[i], a[ randInt(0, i)]);
    chain<int> temp;
    int sizeofarray = numbers;
    for (int n = 0; n < sizeofarray; n++)
        temp.append(n);
    for (int i=1; i<sizeofarray; i++)
    {
        int tempindex = randInt(0, numbers-1);
        int tempnum;
        tempnum = temp.array[tempindex];
        temp.array[tempindex] = temp.array[i];
        temp.array[i] = tempnum;
    }
    return temp;
}

void testchain()
{
    chain<int> a, b, c; //Three empty chains are created
    chain<int> d(10); // A chain containing just one element: 10
    cout << d; // Output is [ 10 ]
    cout << a.length() << endl; // yields 0
    cin >> a; // User types [2 3 7]
    cout << a; // Output is [2 3 7]
    cin >> b; // User types [8 4 2 1]
    c=a; // Assignment
    cout << c; // Output should be [2 3 7]
    cout << a+b << endl; // Output is [2 3 7 8 4 2 1]
    cout << a + 5;//Output is [2 3 7 5]
    chain<int> e( c );//Copy constructor
    cout << e; //Output should be [2 3 7]
    cout << a[1] << endl;//Should printout 3
    c[1]=100; //Should change c
    cout << c; //Should print [2 100 7]
    cout << e; //Should print [2 3 7]
}

void testalgorithms()
{
    algorithmone(250);
    cout << "Algorithm one 250 DONE" << endl;
    algorithmone(500);
    cout << "Algorithm one 500 DONE" << endl;
    algorithmone(1000);
    cout << "Algorithm one 1000 DONE" << endl;
    algorithmone(2000);
    cout << "Algorithm one 2000 DONE" << endl;

    algorithmthree(1000);
    cout << "Algorithm three 1000 DONE" << endl;
    algorithmthree(2000);
    cout << "Algorithm three 2000 DONE" << endl;
    algorithmthree(25000);
    cout << "Algorithm three 25000 DONE" << endl;
    algorithmthree(50000);
    cout << "Algorithm three 50000 DONE" << endl;
    algorithmthree(100000);
    cout << "Algorithm three 100000 DONE" << endl;
    algorithmthree(200000);
    cout << "Algorithm three 200000 DONE" << endl;
    algorithmthree(400000);
    cout << "Algorithm three 400000 DONE" << endl;

    algorithmtwo(1000);
    cout << "Algorithm two 1000 DONE" << endl;
    algorithmtwo(2000);
    cout << "Algorithm two 2000 DONE" << endl;
    algorithmtwo(25000);
    cout << "Algorithm two 25000 DONE" << endl;
    algorithmtwo(50000);
    cout << "Algorithm two 50000 DONE" << endl;
    algorithmtwo(100000);
    cout << "Algorithm two 100000 DONE" << endl;
    algorithmtwo(200000);
    cout << "Algorithm two 200000 DONE" << endl;
    algorithmtwo(400000);
    cout << "Algorithm two 400000 DONE" << endl;
}

void getUserCommand()
{
    int command;
    cout << "Type '1' to test Part 1: The Big Three" << endl;
    cout << "Type '2' to test Part 2: Algorithm Analysis" << endl;
    cout << "Command: ";
    cin >> command;
    if (command == 1)
        testchain();
    else if (command == 2)
        testalgorithms();
    else
        cout << "Unknown command" << endl;
}

int main()
{
    getUserCommand();

    return 0;
}
