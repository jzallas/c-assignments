i) Completed:
Part 1: Done

Part 2: Done
Algorithm one = O(n^2)
Algorithm two = O(n)
Algorithm three = O(n)

Algorithm two and three have similar run times but one is alot longer.

ii) Bugs:
In part one, a question is asked to determine how many inputs the user will insert when attempting to overload >> operator; this was necessary because the stream never enters a fail state so it would loop indefinitely while waiting for the next input.
Part two can take an incredibly long time to complete.


iii) Instructions:
After executing, press 1 then enter to use the test function for the first part of the assignment. Press 2 then enter to use the test function for the second part of the assignment.
