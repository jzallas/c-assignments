//  chain.h
//  Jonathan Zallas
//  Template chain class that creates an dynamic array of any type
//  09/10/12

#ifndef CHAIN_H_INCLUDED
#define CHAIN_H_INCLUDED

#include <iostream>
using namespace std;

template <class T>
class chain
{
public:
    int size; //size of chain
    T *array;

    chain() //constructor
    {
        array = new T[0];
        size = 0;
    };

    chain(const T & element) //constructor with parameter
    {
        array = new T[1];
        array[0] = element;
        size = 1;
    };

    chain(const chain& a) //copy constructor
    {
        array = new T[0];
        size = 0;

        for (int n = 0; n< a.size; n++)
            append(a.array[n]);
    };

    ~chain() //destructor
    {
        delete [] array;
    }

    int length() //length of chain
    {
        return size;
    };

    friend ostream& operator<<(ostream& out, const chain& a) // << overload
    {
        for(int n = 0; n < a.size; n++)
            cout << a.array[n] << " ";
        cout << endl;

        return out;
    };

    friend istream& operator>>(istream& in, chain& a) // >> overload
    {
        T  temporary;
        int amount;

        cout << "How many objects will you insert?" << endl;
        cin >> amount;

        for(int n = 0; n < amount; n++)
        {
            in >> temporary;
            a.append(temporary);
        };

        return in;
    };

    void resize() //adds one empty node to chain
    {
        T* copy = new T[size+1];
        for(int n = 0; n < size; n++)
        {
            copy[n] = array[n];
        }
        size++;
        delete [] array;
        array = copy;

    };

    void append(const T &a) //adds one object to last empty node on chain
    {
        resize();
        array[size-1] = a;
    };

    chain<T> operator +(const chain& a) // + overload with chain parameter
    {
        chain<T> tempchain;
        for(int n = 0; n< size; n++)
            tempchain.append(array[n]);
        for(int n = 0; n< a.size; n++)
            tempchain.append(a.array[n]);

        return tempchain;
    }

    chain<T> operator +(const T& a) // + overload with T parameter
    {
        chain<T> tempchain;
        for(int n = 0; n< size; n++)
            tempchain.append(array[n]);
        tempchain.append(a);

        return tempchain;
    }

    void operator =(const chain& a) // = overload
    {
        delete [] array;
        array = new T[0];
        size = 0;

        for (int n = 0; n< a.size; n++)
            append(a.array[n]);

    }

    T& operator[](int index) // [] overload
    {
        return array[index];
    }
};



#endif // CHAIN_H_INCLUDED
