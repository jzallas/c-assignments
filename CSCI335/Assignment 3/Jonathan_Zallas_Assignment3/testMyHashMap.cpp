//File:         testMyHashMap.cpp
//Author(s):    Jonathan Zallas
//Created on:   October 25, 2012
//Description:  Tests for Templated Hash Map
//Usage:        Uses the Templated Hash map to compute the Adjacent words
//              with the algorithm found in figure 4.72. Outputs
//              general run times and offers to print out the
//              elements of the container used.
//Version:      1.0
//

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "Hashmap.h"
using namespace std;


vector<string> readWords( istream & in )
{
    string oneLine;
    vector<string> v;

    while( in >> oneLine )
        v.push_back( oneLine );

    return v;
}

void computeAdjacentWordsCustom( const vector<string> & words, Hashmap<string,vector<string> > & adjacentWords)
{
    Hashmap<int,vector<string> > wordsByLength;

      // Group the words by their length
    for( unsigned int i = 0; i < words.size( ); i++ )
    {
        wordsByLength.insert(words[i].length())->push_back(words[i]);
    }
      // Work on each group separately
    for (unsigned int m = 0; m < wordsByLength.table.size(); m++) //for every index in the map
    {
        if (wordsByLength.table[m].info == ACTIVE) // if there is an ACTIVE entry
        {
            const vector<string> & groupsWords = wordsByLength.table[m].mappedValue;
            int groupNum = wordsByLength.table[m].key;

            // Work on each position in each group
            for( int i = 0; i < groupNum; i++ )
            {
                // Remove one character in specified position, computing representative.
                // Words with same representatives are adjacent; so first populate
                // a map...
                Hashmap<string,vector<string> > repToWord;

                for( unsigned int j = 0; j < groupsWords.size( ); j++)
                {
                    string rep = groupsWords[j];
                    rep.erase( i, 1 );
                    repToWord.insert(rep)->push_back(groupsWords[j]);
                }

                // and then look for map values with more than one string
                for (unsigned int n = 0; n < repToWord.table.size(); n++)
                {
                    if (repToWord.table[n].info == ACTIVE)
                    {
                        const vector<string> & clique = repToWord.table[n].mappedValue;
                        if( clique.size( ) >= 2 )
                        {
                            for( unsigned int p = 0; p < clique.size( ); p++ )
                                for( unsigned int q = p + 1; q < clique.size( ); q++ )
                                {
                                    adjacentWords.insert(clique[p])->push_back(clique[q]);
                                    adjacentWords.insert(clique[q])->push_back(clique[p]);
                                }
                        }
                    }
                }
            }
        }
    }
}

int main(int argc, char **argv)
{
    //Check input arguments
    if (argc!=3)
    {
        cout << "Usage: " << argv[0] << " <word_file.txt> <initial_size_of_hash_table>" << endl;
        return 0;
    }

    clock_t start, end;

    ifstream fin( argv[1] );
    vector<string> words = readWords( fin );
    cout << "Read the words..." << words.size( ) << endl;
    Hashmap<string,vector<string> > adjacentWordsCustom(atoi(argv[3]));

    string userWord;

    start = clock( );
    computeAdjacentWordsCustom( words, adjacentWordsCustom );
    end = clock( );
    cout << "Elapsed time using templated Hash-map to compute adjacent words: " << double(end-start)/CLOCKS_PER_SEC << endl;
    //OUTPUT ALL ELEMENTS HASH MAP
    cout << "Would you like to display the elements of the map in order? (Y/N) : ";
    cin >> userWord;
    if (userWord == "Y" || userWord == "y")
    {
    for (unsigned int m = 0; m < adjacentWordsCustom.table.size(); m++)
    {
        if (adjacentWordsCustom.table[m].info == ACTIVE)
        {
            cout << adjacentWordsCustom.table[m].key << " : ";
            for (unsigned int n = 0; n < adjacentWordsCustom.table[m].mappedValue.size(); n++)
                cout << adjacentWordsCustom.table[m].mappedValue[n] << " ";
            cout << endl;
        }
    }
    }
    //INPUT WORD AND LOOKUP HASH MAP
    cout << "Please input a word: ";
    cin >> userWord;
    start = clock( );
    cout << userWord << " : ";
    int index = adjacentWordsCustom.findElement(userWord);
    if (index != -1)
    {
    for (unsigned int i = 0; i < adjacentWordsCustom.table[index].mappedValue.size(); i++)
        cout << adjacentWordsCustom.table[adjacentWordsCustom.findElement(userWord)].mappedValue[i] << " ";
    cout << endl;
    }
    else
    cout << "Word does not exist." << endl;
    end = clock( );
    cout << "Elapsed time using templated Hash-map to search: " << double(end-start)/CLOCKS_PER_SEC << endl;

    return 0;
}
