//File:         Hashmap.h
//Author(s):    Jonathan Zallas
//Created on:   October 25, 2012
//Description:  Templated Hash Map
//Usage:        Fully functional Templated Hash map which uses
//              quadratic probing to deal with clustering.
//Version:      1.0
//

#ifndef HASHMAP_H_INCLUDED
#define HASHMAP_H_INCLUDED

#include <iostream>
#include <vector>
#include <string>
using namespace std;

enum STATUS { ACTIVE, EMPTY, DELETED };

int hash(const string &);
int hash(const int &);
bool isPrime(unsigned int);
int nextPrime(unsigned int);

template <class KEYTYPE, class VALUETYPE>
class Hashmap
{
public:
    struct element
    {
        public:
        KEYTYPE key;
        VALUETYPE mappedValue; //vector<string> mappedValue;
        STATUS info;
        KEYTYPE* first;
        VALUETYPE* second;
        element()
        {
            info = EMPTY;
            first = &key;
            second = &mappedValue;
        }
    };
    vector<element> table;
    unsigned int count; // keeps track of how many elements are stored in table
    void emptyHash()
    {
        table.erase(table.begin(),table.end());
    }

    void reHash()
    {
        vector<element> temp(table.begin(), table.end()); //create a temp vector to hold items
        emptyHash(); //clean out hash table
        table.resize(nextPrime(temp.size()+1)); //resize the hash table


        for (unsigned int i = 0; i < temp.size(); i++) //traverse the temp vector and re-add all the ACTIVE
            {
                if (temp[i].info == ACTIVE)
                {
                    addElement(temp[i].key, temp[i].mappedValue);
                    count--;
                }
            }
    }

    bool addElement(const KEYTYPE & key, const VALUETYPE & value)
    {
        if (count >= (table.size()/2)) //if the table is more than half full, rehash
            reHash();

        int index = hash(key) % table.size();

        for (unsigned int j = 0; j <= table.size(); ++j )
        {
            if (table[index].info == EMPTY || table[index].info == DELETED) // if it belongs here, insert it and stop
            {
                table[index].info = ACTIVE; //ACTIVE EMPTY DELETED
                table[index].key = key;
                table[index].mappedValue = value;
                count++;
                return true;
            }
            else if (table[index].key == key) //if it is already here, can't insert
            {
                return false;
            }
            else //new hash with quadratic probe
            {
                index = hash(index +(j*j)) % table.size();
            }
        }
        cout << "unable to insert, table full" << endl;
        return false; //table is full (should never reach this)
    }
    VALUETYPE* addElement(const KEYTYPE & key)
    {
        if (count >= (table.size()/2)) //if the table is more than half full, rehash
            reHash();

        int index = hash(key) % table.size();
        for (unsigned int j = 0; j <= table.size(); ++j )
        {
            if (table[index].info == EMPTY || table[index].info == DELETED) // if it belongs here, insert it and stop
            {
                table[index].info = ACTIVE; //ACTIVE EMPTY DELETED
                table[index].key = key;
                count++;
                return &(table[index].mappedValue);
            }
            else if (table[index].key == key) //if it is already here, can't insert
            {
                return &(table[index].mappedValue);
            }
            else //new hash with quadratic probe
            {
                index = hash(index +(j*j)) % table.size();
            }
        }
        cout << "unable to insert, table full" << endl;
        return NULL; //table is full (should never reach this)
    }
    int findElement(const KEYTYPE & key)
    {
        int index = hash(key) % table.size();

        for (unsigned int j = 0; j <= table.size(); ++j)
        {
            if (table[index].info == ACTIVE && key == table[index].key) // if it is active and the key is the same
            {
                return index; //found element, return the index
            }
            else //new hash with quadratic probe
            {
                index = hash(index +(j*j)) % table.size();
            }
        }
        return (-1); // doesn't exist, return -1
    }
    bool removeElement(const KEYTYPE & key)
    {
        int index = findElement(key);
        if (index == -1)
            return false;

        table[index].info = DELETED;
        count--;
        return true;
    }
    void output()
    {
        for (unsigned int i = 0; i < table.size(); i++)
        {
            if (table[i].info == ACTIVE)
            {
                cout << table[i].key << " : " << table[i].mappedValue << endl;
            }
        }
    }
public:
    //CONSTRUCTORS AND DESTRUCTOR
    Hashmap()
    {
        count = 0;
        table.resize(101);

    }
    Hashmap(unsigned int size)
    {
        count = 0;
            table.resize(nextPrime(size));
    }
    ~Hashmap()
    {
        emptyHash();
    }

    // ******************PUBLIC FUNCTIONS*********************
    // void insert(KEY,VALUE) --> Insert KEY and MAPPED VALUE respectively
    // void remove(KEY)       --> Remove the entry with the corresponding KEY
    // bool lookup(KEY)       --> Return true if KEY is present
    // void outputAll()       --> Displays all KEYS and MAPPED VALUES in the order they are stored
    void insert(const KEYTYPE & key, const VALUETYPE & value)
    {
        addElement(key, value);
    }
    VALUETYPE* insert(const KEYTYPE & key)
    {
        return(addElement(key));
    }
    bool lookup(const KEYTYPE & key)
    {
        if (findElement(key) == -1)
            return false;
        return true;
    }
    void remove(const KEYTYPE & key)
    {
        removeElement(key);
    }
    void outputAll()
    {
        output();
    }
};



#endif // HASHMAP_H_INCLUDED
