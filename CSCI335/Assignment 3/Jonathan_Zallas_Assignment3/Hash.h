//File:         Hash.h
//Author(s):    Jonathan Zallas
//Created on:   October 25, 2012
//Description:  Hashing
//Usage:        Simple hash map used to calculate the relationship
//              between load factor and linear probes
//Version:      1.0
//


#ifndef HASH_H_INCLUDED
#define HASH_H_INCLUDED

#include <iostream>
using namespace std;

class Hash{
    public:
    unsigned int size;
    unsigned int capacity;
    int *table;
    int counter;

    //CONSTRUCTORS AND DESTRUCTOR
    Hash();
    Hash(unsigned int);
    ~Hash();

    //HASH FUNCTIONS
    bool insert(int);
    unsigned int getKey(int);

};
#endif // HASH_H_INCLUDED
