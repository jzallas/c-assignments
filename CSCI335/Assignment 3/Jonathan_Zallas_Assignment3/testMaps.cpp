//File:         testMaps.cpp
//Author(s):    Jonathan Zallas
//Created on:   October 25, 2012
//Description:  Tests for STL MAP
//Usage:        Uses the STL MAP to compute the Adjacent words
//              with the algorithm found in figure 4.72. Outputs
//              general run times and offers to print out the
//              elements of the container used.
//Version:      1.0
//


#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <ctime>
using namespace std;


vector<string> readWords( istream & in )
{
    string oneLine;
    vector<string> v;

    while( in >> oneLine )
        v.push_back( oneLine );

    return v;
}

// Computes a map in which the keys are words and values are vectors of words
// that differ in only one character from the corresponding key.
// Uses an efficient algorithm that is O(N log N) with a map, or
// O(N) is a hash_map is used.
map<string,vector<string> > computeAdjacentWords( const vector<string> & words )
{
    map<string,vector<string> > adjWords;
    map<int,vector<string> > wordsByLength;

      // Group the words by their length
    for( unsigned int i = 0; i < words.size( ); i++ )
    {
        wordsByLength[ words[ i ].length( ) ].push_back( words[ i ] ); //finds the index with the appropriate key length and pushes the word to the vector
    }
      // Work on each group separately
    map<int,vector<string> >::const_iterator itr;
    for( itr = wordsByLength.begin( ); itr != wordsByLength.end( ); ++itr )
    {
        const vector<string> & groupsWords = itr->second;
        int groupNum = itr->first;

        // Work on each position in each group
        for( int i = 0; i < groupNum; i++ )
        {
            // Remove one character in specified position, computing representative.
            // Words with same representatives are adjacent; so first populate
            // a map...
            map<string,vector<string> > repToWord;

            for( unsigned int j = 0; j < groupsWords.size( ); j++ )
            {
                string rep = groupsWords[ j ];
                rep.erase( i, 1 );
                repToWord[ rep ].push_back( groupsWords[ j ] );
            }

            // and then look for map values with more than one string
            map<string,vector<string> >::const_iterator itr2;
            for( itr2 = repToWord.begin( ); itr2 != repToWord.end( ); ++itr2 )
            {
                const vector<string> & clique = itr2->second;
                if( clique.size( ) >= 2 )
                {
                    for( unsigned int p = 0; p < clique.size( ); p++ )
                        for( unsigned int q = p + 1; q < clique.size( ); q++ )
                        {
                            adjWords[ clique[ p ] ].push_back( clique[ q ] );
                            adjWords[ clique[ q ] ].push_back( clique[ p ] );
                        }
                }
            }
        }
    }

    return adjWords;
}
int main(int argc, char **argv)
{
    //Check input arguments
    if (argc!=2)
    {
        cout << "Usage: " << argv[0] << " <word_file.txt>" << endl;
        return 0;
    }

    clock_t start, end;

    ifstream fin( argv[1] );
    vector<string> words = readWords( fin );
    cout << "Read the words..." << words.size( ) << endl;
    map<string,vector<string> > adjacentWords;

    start = clock( );
    adjacentWords = computeAdjacentWords( words );
    end = clock( );
    cout << "Elapsed time using STL Map to compute adjacent words: " << double(end-start)/CLOCKS_PER_SEC << endl;

    map<string,vector<string> >::iterator it;
    string userWord;
    //OUTPUT ALL ELEMENTS STL MAP
    cout << "Would you like to display the elements of the map in order? (Y/N) : ";
    cin >> userWord;
    if (userWord == "Y" || userWord == "y")
    {
    for (it = adjacentWords.begin(); it != adjacentWords.end(); it++)
    {
        cout << (*it).first << " : ";
        for (unsigned int m = 0; m < (*it).second.size(); m++)
        {
            cout << (*it).second[m] << " ";
        }
        cout << endl;
    }
    }
    //INPUT WORD AND LOOKUP STL MAP
    cout << "Please input a word: ";
    cin >> userWord;
    start = clock( );
    cout << userWord << " : ";
    for (unsigned int i = 0; i < adjacentWords[userWord].size(); i++)
        cout << adjacentWords[userWord][i] << " ";
    cout << endl;
    end = clock( );
    cout << "Elapsed time using STL Map to search: " << double(end-start)/CLOCKS_PER_SEC << endl;




    return 0;
}
