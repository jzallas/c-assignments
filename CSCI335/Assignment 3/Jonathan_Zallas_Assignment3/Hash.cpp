//File:         Hash.cpp
//Author(s):    Jonathan Zallas
//Created on:   October 25, 2012
//Description:  Hashing
//Usage:        Simple hash map used to calculate the relationship
//              between load factor and linear probes
//Version:      1.0
//



#include "Hash.h"

Hash::Hash()
    {
        size = 0;
        capacity = 10;
        table= new int[10];
        for (unsigned int i = 0; i < capacity; i++)
            table[i] = 0;
    }

Hash::~Hash()
{
    delete [] table;
    table = NULL;
}

Hash::Hash(unsigned int initialSize)
{
    size = 0;
    capacity = initialSize;
    table= new int[capacity];
    for (unsigned int i = 0; i < capacity; i++)
            table[i] = 0;

}

bool Hash::insert(int data)
{
    unsigned int key = getKey(data);

    if (size == capacity || table[key] == data) // if it is already in the table or table is full, fail to insert
        return false;

    table[key] = data;
    size++;

    return true;
}

unsigned int Hash::getKey(int data)
{
    unsigned int key;
    key = data%capacity;
    if (key < 0) // absolute value
        key = key*(-1);

    for(unsigned int i = 0; i < capacity; i++)//look through entire array until valid spot is found
    {
        counter++;
        if (table[(key+i)%capacity] == 0 || table[(key+i)%capacity] == data)
            return ((key+i)%capacity);
    }
    return key;
}
