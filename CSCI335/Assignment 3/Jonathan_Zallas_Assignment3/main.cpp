//File:         main.cpp
//Author(s):    Jonathan Zallas
//Created on:   October 25, 2012
//Description:  Hashing test
//Usage:        Test using simple Hashmap.
//              Outputs the Load Factor and the number of
//              times it probed linearly. Hashmap is always
//              of 1000 size and in this case only 100/1000
//              of the map is full.
//Version:      1.0
//

#include <iostream>
#include "Hash.h"
#include <string>


using namespace std;

int main()
{
    int max = 1000;
    int test = 100;
    Hash sampleHash(max);
    int index = 1;
    sampleHash.counter = 0;
    for(unsigned int i = 1; i < test+1; i++)
    {
        sampleHash.insert(index);
        index = index + i*i;
    }
    cout << (float)sampleHash.size/(float)sampleHash.capacity << " Load Factor" << endl;
    sampleHash.counter = 0;
    sampleHash.insert(16); //MISS INSERT
    cout << "It probed " << sampleHash.counter << " times." << endl;
    return 0;
}
