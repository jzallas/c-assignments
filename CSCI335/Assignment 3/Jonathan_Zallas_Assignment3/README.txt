Assignment 3
Jonathan Zallas (jzallas@hunter.cuny.edu)

--Hashing(Part I) graph is included (named graph.jpg). As expected, as the load factor of the map increased, the more difficult it was to find a place to insert an element. This is shown on the graph.

--Maps and Hashing (Part II)
A) Implemented the most efficient word puzzle algorithm as found in the text.
B) Created a fully functional Templated Map and used the same algorithm as in A along with this Templated Map instead. Here are some sample values received when attempting to find the change in run time:

Size		Time
50		11.913 sec
100		11.954 sec
800		12.009 sec
1600		11.961 sec
6400		11.961 sec
25600		11.953 sec
819200		11.962 sec

I would expect it to be easier to insert elements as the size goes up (due to less collisions), However, the time remains relatively constant. Therefore it is difficult to determine an exact change in run time.

--To compile just type

         make all


--To run the first program type (Part II.A)
     
	 testMaps <word_file.txt>


--To run the second program type (Part II.B)
     
	 testMyHashMap <word_file.txt> <initial_size_of_hash_table>


--To run the third program type (optional, contains the code from Part I)
     
	 hashing


--To remove object files type

         make clean
