//File:         Hashmap.cpp
//Author(s):    Jonathan Zallas
//Created on:   October 25, 2012
//Description:  Templated Hash Map
//Usage:        Fully functional Templated Hash map which uses
//              quadratic probing to deal with clustering.
//Version:      1.0
//

#include "Hashmap.h"

int hash( const string & key)
{
    int hashVal = 0;

    for( unsigned int i = 0; i < key.length( ); i++ )
        hashVal = 37 * hashVal + key[ i ];

    if (hashVal < 0) //Absolute Value
        return (hashVal*(-1));

    return hashVal;
}

int hash( const int & key)
{
    if (key < 0) //Absolute Value
        return (key*(-1));
    return (key);
}

bool isPrime(unsigned int n)
{
    if( n == 2 || n == 3 )
        return true;

    if( n == 1 || n % 2 == 0 )
        return false;

    for( unsigned int i = 3; i * i <= n; i += 2 )
        if( n % i == 0 )
            return false;

    return true;
}

int nextPrime(unsigned int n)
{
    if( n <= 0 )
        n = 3;

    if( n % 2 == 0 )
        n++;

    for( ; !isPrime( n ); n += 2 )
        ;

    return n;
}
