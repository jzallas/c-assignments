Assignment 5
Jonathan Zallas (jzallas@hunter.cuny.edu)

Notes -
I have included two sample text files which represent an acyclic directed graphs.

--Part A) Complete

--Part B) Complete
The algorithm I implemented still has the normal Dijkstra limitation of only being able to use positive edge weights.

--Part C) Complete
Calculates the maximum possible flow between first vector and second vector. This is assuming the graph is directed.

At first I misunderstood this part as asking for the longest path. Subsequentally, I wrote an algorithm to calculate the longest path (assuming the graph is acyclic). It is included and it works, but it is commented out.

--Part D) Complete
Uses Dijkstra to find the shortest path then uses that path as an Augmented Path. Adjusts a Residual Graph and Flow Graph accordingly, and repeats until there is no longer a path between the source and the destination. The graph must be acylic and directed or this will not work.


--To compile just type

         make all


--To run the first program type (Part II.A)
     
	 testGraph


--To remove object files type

         make clean
