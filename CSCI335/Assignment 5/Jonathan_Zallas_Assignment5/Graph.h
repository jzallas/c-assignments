//File:         Graph.h
//Author(s):    Jonathan Zallas
//Created on:   December 11, 2012
//Description:  Graph
//Usage:        Graph represented with an adjaceny list
//              implementing Dijkstra and NetworkFlow algorithms
//Version:      1.0
//

#ifndef GRAPH_H_INCLUDED
#define GRAPH_H_INCLUDED

#include <iostream>
#include <vector>
#include <queue>

#define INFINITY 999999999

using namespace std;

class graph
{
    private:
    int totalVertices;
    struct Vertex
    {
        struct Edge
        {
            Vertex* next;
            float weight;
        };
        int index;
        float distance; //for dijkstra
        Vertex* previous; // for dijkstra
        vector<Edge> edges;

        Vertex()
        {
            distance = INFINITY;
            previous = NULL;
        }

        void add(Vertex* x, float newWeight)
        {
            Edge temp;
            temp.next = x;
            temp.weight = newWeight;
            edges.push_back(temp);
        }
    };
    class CompareVertices //for priority queue
    {
        public:
        bool operator()(Vertex* v1, Vertex* v2)
        {
            if (v2->distance < v1->distance)
                return true;
            return false;
        }
    };
    class CompareVerticesMAX //for priority queue
    {
        public:
        bool operator()(Vertex* v1, Vertex* v2)
        {
            if (v1->distance < v2->distance)
                return true;
            return false;
        }
    };
    vector<Vertex*> vertices; //store the Vertex pointers in here for the sake of easy deletion
    vector<Vertex*> neighbors(Vertex* x);


    public:
    Vertex* findVertex(int index);
    graph();
    ~graph();
    bool adjacent(Vertex* x, Vertex* y);
    bool addEdge(int firstIndex, int secondIndex, float weight);
    Vertex* addVertex(int newIndex);
    int getSize();

    int getVertexIndex(Vertex* x);
    void setVertexIndex(Vertex* x, int newIndex);

    float getEdgeWeight(Vertex* x, Vertex* y);
    void setEdgeWeight(Vertex* x, Vertex* y, float newWeight);

    void dijkstra(Vertex* x); //dijkstra algorithm
    float maximumFlow(Vertex* x, Vertex* y); //calculates maximum flow for a given path x to y

    void longestPath(Vertex* x); //calculates longest path
    float networkFlow(graph & flowGraph); //network flow algorithm, takes a graph as a parameter and turns the graph into a flow graph
};


#endif // GRAPH_H_INCLUDED
