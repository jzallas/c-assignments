//File:         Graph.cpp
//Author(s):    Jonathan Zallas
//Created on:   December 11, 2012
//Description:  Graph
//Usage:        Graph represented with an adjaceny list
//              implementing Dijkstra and NetworkFlow algorithms
//Version:      1.0
//

#include "Graph.h"

graph :: graph()
{
    totalVertices = 0;
}

graph :: ~graph() //destructor; deletes all the vertices in the graph
{
    totalVertices = 0;
    for (unsigned int i = 0; i < vertices.size(); i++)
    {
        if (vertices[i] != NULL)
        {
            delete vertices[i];
            vertices[i] = NULL;
        }
    }
}

bool graph :: adjacent(Vertex* x, Vertex* y)
{
    for (unsigned int i = 0; i < x->edges.size(); i++)
    {
        if (x->edges[i].next == y)
            return true;
    }
    return false;
}

vector<graph::Vertex*> graph :: neighbors(Vertex* x)
{
    vector<Vertex*> temp;
    for (unsigned int i = 0; i < x->edges.size(); i++)
        temp.push_back(x->edges[i].next);
    return temp;
}

bool graph :: addEdge(int firstIndex, int secondIndex, float newWeight)
{
    Vertex* x = findVertex(firstIndex);
    Vertex* y = findVertex(secondIndex);
    for (unsigned int i = 0; i < x->edges.size(); i++)
    {
        if (x->edges[i].next == y)
            return false;
    }

    x->add(y, newWeight);
    return true;
}

graph::Vertex* graph :: addVertex(int newIndex)
{
    Vertex* temp = new Vertex;
    setVertexIndex(temp, newIndex);
    vertices.push_back(temp);
    totalVertices++;
    return temp;
}

int graph :: getVertexIndex(Vertex* x)
{
    return x->index;
}

void graph :: setVertexIndex(Vertex* x, int newIndex)
{
    x->index = newIndex;
}

float graph :: getEdgeWeight(Vertex* x, Vertex* y)
{
    for (unsigned int i = 0; i < x->edges.size(); i++)
    {
        if (x->edges[i].next == y)
            return x->edges[i].weight;
    }
    return -1;
}

void graph :: setEdgeWeight(Vertex* x, Vertex* y, float newWeight)
{
    for (unsigned int i = 0; i < x->edges.size(); i++)
    {
        if (x->edges[i].next == y)
            x->edges[i].weight = newWeight;
    }
}

graph :: Vertex* graph :: findVertex(int index)
{
    for (unsigned int i = 0; i < vertices.size(); i++)
    {
        if (vertices[i]->index == index)
            return vertices[i];
    }
    return NULL;
}

void graph :: dijkstra(Vertex* x)
{
    for (unsigned int i = 0; i < vertices.size(); i++)
    {
        vertices[i]->distance = INFINITY;
        vertices[i]->previous = NULL;
    }

    x->distance = 0;
    priority_queue<Vertex*, vector<Vertex*>, CompareVertices> Q;
    for (unsigned int i = 0; i < vertices.size(); i++)
    {
        Q.push(vertices[i]);
    }

    while(!Q.empty())
    {
        //FIND THE SMALLEST UNKNOWN DISTANCE VERTEX
        Vertex* u = Q.top();
        Q.pop();

        if (u->distance == INFINITY)
            break;

        for (unsigned int i = 0; i < u->edges.size(); i++)
        {
            float alt = u->distance + u->edges[i].weight;
            if (alt < u->edges[i].next->distance)
            {
                u->edges[i].next->distance = alt;
                u->edges[i].next->previous = u;
                //reorder priority queue
                vector<Vertex*> temp;
                while (!Q.empty())
                {
                    temp.push_back(Q.top());
                    Q.pop();
                }
                while (!temp.empty())
                {
                    Q.push(temp.back());
                    temp.pop_back();
                }
            }
        }
    }
}

void graph :: longestPath(Vertex* x)
{
    ///FIRST SET ALL PATHS TO NEGATIVE 1
    for (unsigned int i = 0; i < vertices.size(); i++)
    {
        vertices[i]->distance = -1;
        vertices[i]->previous = NULL;
    }

    ///THE ORIGIN IS OBVIOUSLY ZERO DISTANCE SO SET IT TO ZERO
    x->distance = 0;

    ///CREATE A QUEUE THAT ALWAYS PUTS LARGER DISTANCES FIRST
    priority_queue<Vertex*, vector<Vertex*>, CompareVerticesMAX> Q;

    ///LOAD THE QUEUE WITH ALL NODES
    for (unsigned int i = 0; i < vertices.size(); i++)
        Q.push(vertices[i]);



    while(!Q.empty())
    {
        ///FIND THE LARGEST UNKNOWN DISTANCE VERTEX AND EXTRACT FROM QUEUE
        Vertex* u = Q.top();
        Q.pop();

        ///IF ENCOUNTERING A NODE THAT HAS A DISTANCE -1, THEN IT IS ISOLATED
        ///AND CANNOT BE USED
        if (u->distance == -1)
        {
            break;
        }

        ///OTHERWISE, FOR EACH EDGE OF THIS VERTEX
        for (unsigned int i = 0; i < u->edges.size(); i++)
        {
            ///CALCULATE THE KNOWN DISTANCE OF THIS VERTEX + THE DISTANCE OF THE EDGE
            float alt = u->distance + u->edges[i].weight;

            ///IF THE DISTANCE OF THE NEXT VERTEX IS LESS THAN THE CALCULATED DISTANCE
            if (alt > u->edges[i].next->distance)
            {
                ///UPDATE THE NEXT VERTEX'S DISTANCE
                u->edges[i].next->distance = alt;

                ///UPDATE WHICH NODE THE DISTANCE CAME FROM
                u->edges[i].next->previous = u;

                ///REORDER PRIORITY QUEUE TO PUT NEW LARGEST AT TOP
                vector<Vertex*> temp;
                bool exists = false;
                while (!Q.empty())
                {
                    if (Q.top() == u->edges[i].next)
                        exists = true;
                    temp.push_back(Q.top());
                    Q.pop();
                }
                ///IF IT WAS VISITED BEFORE, PUT THE NODE BACK IN THE QUEUE
                if (!exists)
                    Q.push(u->edges[i].next);
                while (!temp.empty())
                {
                    Q.push(temp.back());
                    temp.pop_back();
                }
            }
        }
    }
}

float graph :: maximumFlow(Vertex* x, Vertex* y)
{
    float min = INFINITY;
    Vertex* temp = y;
    if (y->distance != INFINITY)
    {
        while (temp != x)
        {
            if (temp->previous == NULL)
                return 0;
            if ((temp->distance - temp->previous->distance) < min)
                min = temp->distance - temp->previous->distance;
            temp = temp->previous;
        }
    }
    else
        return 0;
    return min;
}

float graph :: networkFlow(graph & flowGraph)
{
    graph residualGraph;
    ///CREATE ALL THE VERTICES
    for (unsigned int i = 0; i < vertices.size(); i++)
    {
        flowGraph.addVertex(vertices[i]->index);
        residualGraph.addVertex(vertices[i]->index);
    }

    ///CREATE ALL THE EDGES
    for (unsigned int i = 0; i < vertices.size(); i++)///FOR EACH VERTEX
    {
        for (unsigned int j = 0; j < vertices[i]->edges.size(); j++) ///GO THRU IT'S EDGES
        {
            ///ADD THIS EDGE TO THE FLOW GRAPH WITH 0 WEIGHT
            flowGraph.addEdge(vertices[i]->index, vertices[i]->edges[j].next->index, 0);
            ///ADD THIS EDGE TO THE RESIDUAL GRAPH WITH IDENTICAL WEIGHTS
            residualGraph.addEdge(vertices[i]->index, vertices[i]->edges[j].next->index, vertices[i]->edges[j].weight);
        }
    }

    cout << "Indicate the source Vertex: ";
    int start, end;
    float maxflow;
    float capacity = 0;
    cin >> start;
    cout << "Indicate the destination Vertex: ";
    cin >> end;

    ///DO DIJKSTRA TO UPDATE THE PATHS
    residualGraph.dijkstra(residualGraph.findVertex(start));

    if (residualGraph.findVertex(end)->distance == INFINITY) ///THERE WAS NEVER A PATH TO BEGIN WITH
        return capacity;

    while(residualGraph.findVertex(end)->distance != INFINITY)///IF THE DESTINATION IS REACHABLE
    {
        ///FIND THE MAX FLOW OF THE SHORTEST PATH
        maxflow = maximumFlow(residualGraph.findVertex(start), residualGraph.findVertex(end));

        Vertex* temp = residualGraph.findVertex(end);
        capacity = capacity + maxflow;
        while (temp != residualGraph.findVertex(start)) ///GO BACKWARDS UNTIL YOU GET TO THE FIRST NODE
        {
            for (unsigned int k = 0; k < temp->previous->edges.size(); k++) ///FOR EVERYNODE, FIND THE APPROPRIATE EDGE
            {
                if (temp->previous->edges[k].next == temp) ///IF THIS IS THE CORRECT EDGE
                    {
                        /// SUBTRACT MAXFLOW FROM EDGE
                        temp->previous->edges[k].weight = temp->previous->edges[k].weight - maxflow;

                        ///ADD WHAT WAS SUBTRACTED TO THE SAME EDGE IN THE FLOW GRAPH
                        Vertex* flowtemp = flowGraph.findVertex(temp->previous->index);
                        for (unsigned int m = 0; m < flowtemp->edges.size(); m++)
                        {
                            if (flowtemp->edges[m].next->index == temp->index)
                            {
                                flowtemp->edges[m].weight = flowtemp->edges[m].weight + maxflow;
                                break;
                            }
                        }

                        ///IF WE MADE AN EDGE 0, ERASE THE EDGE
                        if (temp->previous->edges[k].weight == 0)
                            temp->previous->edges.erase(temp->previous->edges.begin()+k);
                        break;
                    }
            }
            temp = temp->previous;
        }
        residualGraph.dijkstra(residualGraph.findVertex(start));
    }
    return capacity;
}

int graph :: getSize()
{
    return totalVertices;
}
