//File:         main.cpp
//Author(s):    Jonathan Zallas
//Created on:   December 11, 2012
//Description:  Graph
//Usage:        Program to test graph functionality
//              Loads a graph from a file, calculates the
//              shortest paths, finds the max flow between
//              A and B, and outputs a Flow Graph to a txt file
//Version:      1.0
//

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stack>
#include "Graph.h"

using namespace std;

void loadFile(graph & loadGraph)
{
    string filename;
    cout << "Please enter a filename to load: ";
    cin >> filename;
    ifstream openfile(filename.c_str());
    int lineNumber = 0;
    string line;

    while (openfile.good())
    {
        getline(openfile, line);
        //Increment line number
        lineNumber++;
        stringstream ss;
        ss << line;

        //EXTRACT FIRST NUMBER AND CREATE VERTICES
        int numOfVertices;
        if(ss && lineNumber == 1)
        {
            ss >> numOfVertices;
            for (int i = 0; i < numOfVertices; i++)
                loadGraph.addVertex(i);
        }
        else
        {
            int currIndex;
            ss >> currIndex;
            while(ss)
            {
                int edgeIndex = -1;
                ss >> edgeIndex;
                if (edgeIndex != -1)
                {
                    float edgeWeight;
                    ss >> edgeWeight;
                    if(ss && lineNumber > 1)
                    {
                        loadGraph.addEdge(currIndex, edgeIndex, edgeWeight);
                    }
                }
            }
        }
    }
    openfile.close();
};

void writeFile(graph & loadGraph)
{
    ofstream writefile;
    writefile.open ("FlowGraph.txt");
    writefile << loadGraph.getSize() << endl;
    for (int i = 0; i < loadGraph.getSize() ; i++)
    {
        writefile << loadGraph.findVertex(i)->index;
        if (loadGraph.findVertex(i)->edges.size() > 0)
        {
            for (unsigned int j = 0; j < loadGraph.findVertex(i)->edges.size(); j++)
                writefile << " " << loadGraph.findVertex(i)->edges[j].next->index << " " << loadGraph.findVertex(i)->edges[j].weight;
            writefile << " -1" << endl;
        }
    }

    cout << endl << endl << "Outputted to FlowGraph.txt" << endl;

    writefile.close();
}

void shortestPath(graph & loadGraph)
{
    cout << endl <<"*****Dijkstra Algorithm - Shortest Path*****" << endl << "What vertex would you like to start from? ";
    int start, end, itr;
    cin >> start;
    loadGraph.dijkstra(loadGraph.findVertex(start));
    cout << "What vertex would you like to end at? ";
    cin >> end;

    stack<int> tempStack; //ONLY USED TO REVERSE THE ORDER OF PATH : FILO STRUCTURE
    if (loadGraph.findVertex(end)->distance != INFINITY)
        cout << "The shortest path to Vertex " << end << " is " << loadGraph.findVertex(end)->distance << "." << endl;
    else
    {
        cout << "There is no path." << endl;
        return;
    }
    tempStack.push(end);
    itr = end;
    while (tempStack.top() != start)
    {
        if (loadGraph.findVertex(itr)->previous != NULL)
        {
            itr = loadGraph.findVertex(itr)->previous->index;
            tempStack.push(itr);
        }
    }

    cout << "The vertices along the path are: " << endl;
    cout << "   ";
    while (!tempStack.empty())
    {
        cout << tempStack.top();
        tempStack.pop();
        if (!tempStack.empty())
            cout << " -> ";
    }
    cout << endl;

};

void longestPath(graph & loadGraph)
{
    cout << "What vertex would you like to start from? ";
    int start, end, itr;
    cin >> start;
    loadGraph.longestPath(loadGraph.findVertex(start));
    cout << "What vertex would you like to end at? ";
    cin >> end;

    stack<int> tempStack; //ONLY USED TO REVERSE THE ORDER OF PATH : FILO STRUCTURE
    if (loadGraph.findVertex(end)->distance != -1)
        cout << "The longest path to Vertex " << end << " is " << loadGraph.findVertex(end)->distance << "." << endl;
    else
    {
        cout << "There is no path." << endl;
        return;
    }
    tempStack.push(end);
    itr = end;
    while (tempStack.top() != start)
    {
        if (loadGraph.findVertex(itr)->previous != NULL)
        {
            itr = loadGraph.findVertex(itr)->previous->index;
            tempStack.push(itr);
        }
    }

    cout << "The vertices along the path are: " << endl;
    cout << "   ";
    while (!tempStack.empty())
    {
        cout << tempStack.top();
        tempStack.pop();
        if (!tempStack.empty())
            cout << " -> ";
    }
    cout << endl;

};

void maxflow(graph & loadGraph)
{
    int first, second, max;
    cout << endl << "*****Maximum Flow between A and B*****" << endl << "Enter first Vertex :";
    cin >> first;
    cout << "Enter second Vertex :";
    cin >> second;

    max = loadGraph.maximumFlow(loadGraph.findVertex(first),loadGraph.findVertex(second));
    if (max == 0)
        cout << "Could not calculate a maximum flow with the given points." << endl;
    else
        cout << "The maximum flow between " << first << " and " << second << " is " << max << "." << endl;

}

void netflow(graph & loadGraph)
{
    graph flowGraph;
    cout << endl << "*****Network Flow Algorithm - Flow Graph*****" << endl;
    float capacity = loadGraph.networkFlow(flowGraph);
    if(capacity)
    {
        cout << "The maximum flow for the graph is " << capacity << "." <<  endl << endl;

        cout << flowGraph.getSize() << endl;
        for (int i = 0; i < flowGraph.getSize() ; i++)
        {
            cout << flowGraph.findVertex(i)->index;
            if (flowGraph.findVertex(i)->edges.size() > 0)
            {
                for (unsigned int j = 0; j < flowGraph.findVertex(i)->edges.size(); j++)
                    cout << "  " << flowGraph.findVertex(i)->edges[j].next->index << " " << flowGraph.findVertex(i)->edges[j].weight;
                cout << endl;
            }
        }
        writeFile(flowGraph);
    }
    else
        cout << "Could not compute network flow graph." << endl;
}

int main()
{
    ///PART A
    ///
    graph sample;
    loadFile(sample);

    ///PART B
    ///
    shortestPath(sample);
    //longestPath(sample);

    ///PART C
    ///
    maxflow(sample);

    ///PART D
    ///
    netflow(sample);


    return 0;
}
