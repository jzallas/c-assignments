//File:         BinomialQueue.h
//Author(s):    Jonathan Zallas
//Created on:   November 10, 2012
//Description:  Priority Queue
//Usage:        A binomial queue linked with a hashmap
//              Has additional remove and find functions
//Version:      1.0
//
#ifndef BINOMIAL_QUEUE_H
#define BINOMIAL_QUEUE_H

#include <iostream>
#include <vector>
#include <stack>
#include "Hashmap.h"
#include "dsexceptions.h"
using namespace std;

// Binomial queue class
//
// CONSTRUCTION: with no parameters
//
// ******************PUBLIC OPERATIONS*********************
// void insert( x, y )    --> Insert x, update LineNumber with y
// deleteMin( )           --> Return and remove smallest item
// bool remove( x )       --> Percolates x to root and removes x
// Comparable findMin( )  --> Return smallest item
// bool isEmpty( )        --> Return true if empty; else false
// void makeEmpty( )      --> Remove all items
// void merge( rhs )      --> Absorb rhs into this heap
// BinomialNode* find( x )--> Returns a pointer to the node containing x
// ******************ERRORS********************************
// Throws UnderflowException as warranted

template <typename Comparable>
class BinomialQueue
{
public:
    int comparisonCounter;
    int assignmentCounter;
    BinomialQueue( ) : theTrees( DEFAULT_TREES )
    {
        for( unsigned int i = 0; i < theTrees.size( ); i++ )
            theTrees[ i ] = NULL;
        currentSize = 0;
        assignmentCounter = 0;
        comparisonCounter = 0;
    }

    BinomialQueue( const Comparable & item ) : theTrees( 1 ), currentSize( 1 )
    {
        theTrees[ 0 ] = new BinomialNode( item, NULL, NULL );
    }

    BinomialQueue( const BinomialQueue & rhs ) : currentSize( 0 )
    {
        *this = rhs;
    }

    ~BinomialQueue( )
    {
        makeEmpty( );
    }

    /**
     * Return true if empty; false otherwise.
     */
    bool isEmpty( ) const
    {
        return currentSize == 0;
    }

    /**
     * Returns minimum item.
     * Throws UnderflowException if empty.
     */
    const Comparable & findMin( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException( );

        return theTrees[ findMinIndex( ) ]->element;
    }

    /**
     * Insert item x into the priority queue; allows duplicates.
     */
    void insert( const Comparable & x )
    {
        BinomialQueue tmp( x );
        merge( tmp );
    }

    /**
     * Insert item x into the priority queue, updates line occurrence with y; does not allow duplicates.
     */
    void insert( const Comparable & x, const int & y )
    {
        BinomialNode* temp = find(x); //temp becomes pointer to node or NULL
        if (temp == NULL) //if the node doesnt exist yet
        {
            BinomialQueue tmp( x );
            table.insert( x, tmp.theTrees[0] ); //insert key and pointer to node in hashmap
            tmp.theTrees[0]->lineNumber.push_back(y);
            merge( tmp );
        }
        else //if it exists just update the line number
        {
            if (temp->lineNumber.back() != y) //look at last element and see if has line, if not, update
                temp->lineNumber.push_back(y);
        }
    }

    /**
     * Remove the smallest item from the priority queue.
     * Throws UnderflowException if empty.
     */
    Comparable deleteMin( )
    {

        BinomialNode* temp = find(findMin());
        if (temp!= NULL)
        {
            cout << "Deleted " << temp->element << " which was located on line(s)";
            for (unsigned int i = 0; i < temp->lineNumber.size(); i++)
                cout << " " << temp->lineNumber[i];
            cout << endl;
        }
        Comparable x;
        deleteMin( x );
        table.remove(x);
        return x;
    }

    bool remove( const Comparable & x)
    {

        if( isEmpty( ) )
            throw UnderflowException( );

        if(!table.contains(x))
        {
            return false;
        }
        BinomialNode* nodeToRemove = find(x);
        if (percolate(nodeToRemove))
        {
            table.remove(x);
            int index;
            for (unsigned int i = 0; i< theTrees.size(); i++)
            {
                if (theTrees[i] == nodeToRemove)
                {
                    index = i;
                    break;
                }
            }

            BinomialNode *oldRoot = theTrees[ index ];
            BinomialNode *deletedTree = oldRoot->leftChild;
            delete oldRoot;

            // Construct H''
            BinomialQueue deletedQueue;
            deletedQueue.theTrees.resize( index + 1 );
            deletedQueue.currentSize = ( 1 << index ) - 1;
            for( int j = index - 1; j >= 0; j-- )
            {
                deletedQueue.theTrees[ j ] = deletedTree;
                deletedTree = deletedTree->nextSibling;
                deletedQueue.theTrees[ j ]->nextSibling = NULL;
            }

            // Construct H'
            theTrees[ index ] = NULL;
            currentSize -= deletedQueue.currentSize + 1;

            merge( deletedQueue );

            return true;
        }
        cout << "could not percolate" << endl;
        return false;
    }

    /**
     * Remove the minimum item and place it in minItem.
     * Throws UnderflowException if empty.
     */
    void deleteMin( Comparable & minItem )
    {
        if( isEmpty( ) )
            throw UnderflowException( );

        int minIndex = findMinIndex( );
        minItem = theTrees[ minIndex ]->element;

        BinomialNode *oldRoot = theTrees[ minIndex ];
        BinomialNode *deletedTree = oldRoot->leftChild;
        delete oldRoot;

        // Construct H''
        BinomialQueue deletedQueue;
        deletedQueue.theTrees.resize( minIndex + 1 );
        deletedQueue.currentSize = ( 1 << minIndex ) - 1;
        for( int j = minIndex - 1; j >= 0; j-- )
        {
            deletedQueue.theTrees[ j ] = deletedTree;
            deletedTree = deletedTree->nextSibling;
            deletedQueue.theTrees[ j ]->nextSibling = NULL;
        }

        // Construct H'
        theTrees[ minIndex ] = NULL;
        currentSize -= deletedQueue.currentSize + 1;

        merge( deletedQueue );
    }

    /**
     * Make the priority queue logically empty.
     */
    void makeEmpty( )
    {
        currentSize = 0;
        for( unsigned int i = 0; i < theTrees.size( ); i++ )
            makeEmpty( theTrees[ i ] );
        table.makeEmpty();
    }

    /**
     * Merge rhs into the priority queue.
     * rhs becomes empty. rhs must be different from this.
     * Exercise 6.35 needed to make this operation more efficient.
     */
    void merge( BinomialQueue & rhs )
    {
        if( this == &rhs )    // Avoid aliasing problems
            return;
        comparisonCounter++;

        currentSize += rhs.currentSize;
        assignmentCounter++;

        if( currentSize > capacity( ) )
        {
            int oldNumTrees = theTrees.size( );
            int newNumTrees = max( theTrees.size( ), rhs.theTrees.size( ) ) + 1;
            theTrees.resize( newNumTrees );
            for( int i = oldNumTrees; i < newNumTrees; i++ )
            {
                theTrees[ i ] = NULL;
                assignmentCounter++;
            }
            comparisonCounter++;
            assignmentCounter+=2;
        }
        comparisonCounter++;

        BinomialNode *carry = NULL;
        assignmentCounter++;
        for(unsigned int i = 0, j = 1; j <= currentSize; i++, j *= 2 )
        {
            comparisonCounter++;

            BinomialNode *t1 = theTrees[ i ];
            BinomialNode *t2 = i < rhs.theTrees.size( ) ? rhs.theTrees[ i ] : NULL;

            int whichCase = t1 == NULL ? 0 : 1;
            whichCase += t2 == NULL ? 0 : 2;
            whichCase += carry == NULL ? 0 : 4;

            assignmentCounter+=5;
            comparisonCounter+=4;

            switch( whichCase )
            {
            case 0: /* No trees */
            case 1: /* Only this */
                break;
            case 2: /* Only rhs */
                theTrees[ i ] = t2;
                rhs.theTrees[ i ] = NULL;
                assignmentCounter+=2;
                break;
            case 4: /* Only carry */
                theTrees[ i ] = carry;
                carry = NULL;
                assignmentCounter+=2;
                break;
            case 3: /* this and rhs */
                carry = combineTrees( t1, t2 );
                theTrees[ i ] = rhs.theTrees[ i ] = NULL;
                assignmentCounter+=3;
                break;
            case 5: /* this and carry */
                carry = combineTrees( t1, carry );
                theTrees[ i ] = NULL;
                assignmentCounter+=2;
                break;
            case 6: /* rhs and carry */
                carry = combineTrees( t2, carry );
                rhs.theTrees[ i ] = NULL;
                assignmentCounter+=2;
                break;
            case 7: /* All three */
                theTrees[ i ] = carry;
                carry = combineTrees( t1, t2 );
                rhs.theTrees[ i ] = NULL;
                assignmentCounter+=3;
                break;
            }
        }

        for( unsigned int k = 0; k < rhs.theTrees.size( ); k++ )
        {
            comparisonCounter++;
            rhs.theTrees[ k ] = NULL;
            assignmentCounter++;
        }
        rhs.currentSize = 0;
        assignmentCounter++;
    }

    const BinomialQueue & operator= ( const BinomialQueue & rhs )
    {
        if( this != &rhs )
        {
            makeEmpty( );
            theTrees.resize( rhs.theTrees.size( ) );  // Just in case
            for( unsigned int i = 0; i < rhs.theTrees.size( ); i++ )
                theTrees[ i ] = clone( rhs.theTrees[ i ] );
            currentSize = rhs.currentSize;
        }
        return *this;
    }

    struct BinomialNode
    {
        Comparable element;
        vector<int> lineNumber;
        BinomialNode *leftChild;
        BinomialNode *nextSibling;

        BinomialNode( const Comparable & theElement, BinomialNode *lt, BinomialNode *rt ) : element( theElement ), leftChild( lt ), nextSibling( rt )
        {
        }
    };

    enum { DEFAULT_TREES = 1 };

    vector<BinomialNode *> theTrees;  // An array of tree roots
    unsigned int currentSize;         // Number of items in the priority queue
    HashMap<Comparable, BinomialNode*> table;  //holds pair of (key, pointer to node in theTrees)


    /**
     * Find index of tree containing the smallest item in the priority queue.
     * The priority queue must not be empty.
     * Return the index of tree containing the smallest item.
     */
    int findMinIndex( ) const
    {
        unsigned int i;
        unsigned int minIndex;

        for( i = 0; theTrees[ i ] == NULL; i++ )
            ;

        for( minIndex = i; i < theTrees.size( ); i++ )
            if( theTrees[ i ] != NULL && theTrees[ i ]->element < theTrees[ minIndex ]->element )
                minIndex = i;

        return minIndex;
    }

    /**
     * Searches a subtree to see if the
     * parent of a node is in that tree.
     * Returns the parent.
     */
    BinomialNode* findParent(BinomialNode* x, BinomialNode *root)
    {
        stack<BinomialNode*> s;
        BinomialNode *current = root;
        bool done = false;
        while (!done)
        {
            if (current)
            {
                s.push(current);
                current = current->leftChild;
            }
            else
            {
                if (s.empty())
                {
                    done = true;
                }
                else
                {
                    current = s.top();
                    s.pop();
                    if (current->leftChild == x || current->nextSibling == x)
                        return current;
                    current = current->nextSibling;
                }
            }
        }
        return NULL;
    }

    /**
     * Percolates a node to the root
     * regardless of whether or not it
     * belongs there.
     */
    bool percolate(BinomialNode * x)
    {
        for (unsigned int i = 0; i < theTrees.size(); i++) //go through each subtree
        {
            if (theTrees[i] == x) // if what we want to delete is at the root
            {
                return true;
            }
            else if (theTrees[i] != NULL) //if there is a tree at that index
            {
                BinomialNode* parent = findParent(x,theTrees[i]);//search that subtree for the element and find its parent

                if (parent != NULL) //if there is a parent, need to percolate up
                {
                    BinomialNode* grandparent = findParent(parent, theTrees[i]); //check to see if the parent has a grandparent
                    if (grandparent == NULL)//if the parent doesn't have a grandparent, then it is a root and only one swap is needed before done percolating
                    {
                        if (parent->leftChild == x) //if the element is the left child of the parent
                        {
                            BinomialNode* temp;

                            temp = parent->nextSibling;
                            parent->leftChild = x->leftChild;
                            parent->nextSibling = x->nextSibling;
                            x->nextSibling = temp;
                            x->leftChild = parent;
                            theTrees[i] = x;
                            return true;

                        }
                        else //if the element is the next sibling of the parent
                        {
                            BinomialNode* temp;

                            temp = parent->leftChild;
                            parent->leftChild = x->leftChild;
                            parent->nextSibling = x->nextSibling;
                            x->leftChild = temp;
                            x->nextSibling = parent;
                            theTrees[i] = x;
                            cout << "percolated to root" << endl;
                            return true;
                        }
                    }
                    else//2)parent is a child of another node
                    {
                        if (parent->leftChild == x) //if the element is the left child of the parent
                        {
                            BinomialNode* temp;

                            temp = parent->nextSibling;
                            parent->leftChild = x->leftChild;
                            parent->nextSibling = x->nextSibling;
                            x->nextSibling = temp;
                            x->leftChild = parent;
                            if (grandparent->leftChild == parent)
                                grandparent->leftChild = x;
                            else
                                grandparent->nextSibling = x;
                            i--; //run the loop again on the same index until at top

                        }
                        else //if the element is the next sibling of the parent
                        {
                            BinomialNode* temp;

                            temp = parent->leftChild;
                            parent->leftChild = x->leftChild;
                            parent->nextSibling = x->nextSibling;
                            x->leftChild = temp;
                            x->nextSibling = parent;
                            if (grandparent->leftChild == parent)
                                grandparent->leftChild = x;
                            else
                                grandparent->nextSibling = x;
                            i--; //run the loop again on the same index until at top
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * return the pointer that points to the node that holds the key
     * or NULL if key is not in the queue.
     */
    BinomialNode* find(const Comparable & key)
    {
        int index = table.findElement(key);
        if (index >=0)
        {
            if (table.array[index].element == key)
            {
                return table.array[index].value;
            }
        }
        return NULL;
    }

    /**
     * Return the capacity.
     */
    unsigned int capacity( ) const
    {
        return ( 1 << theTrees.size( ) ) - 1;
    }

    /**
     * Return the result of merging equal-sized t1 and t2.
     */
    BinomialNode * combineTrees( BinomialNode *t1, BinomialNode *t2 )
    {
        if( t2->element < t1->element )
            return combineTrees( t2, t1 );
        t2->nextSibling = t1->leftChild;
        t1->leftChild = t2;
        return t1;
    }

    /**
     * Make a binomial tree logically empty, and free memory.
     */
    void makeEmpty( BinomialNode * & t )
    {
        if( t != NULL )
        {
            makeEmpty( t->leftChild );
            makeEmpty( t->nextSibling );
            delete t;
            t = NULL;
        }
    }

    /**
     * Internal method to clone subtree.
     */
    BinomialNode * clone( BinomialNode * t ) const
    {
        if( t == NULL )
            return NULL;
        else
            return new BinomialNode( t->element, clone( t->leftChild ), clone( t->nextSibling ) );
    }
};

#endif
