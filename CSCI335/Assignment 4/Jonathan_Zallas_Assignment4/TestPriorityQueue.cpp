//File:         TestPriorityQueue.cpp
//Author(s):    Jonathan Zallas
//Created on:   November 10, 2012
//Description:  Test Priority Queue
//Usage:        Test for Priority Queue linked
//              with a Hashmap
//Version:      1.0
//
#include "BinomialQueue.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
using namespace std;

int main( )
{
    BinomialQueue<string> test;
    ifstream openfile("wordsHW2.txt");
    int lineNumber = 0;
    string line;
    test.comparisonCounter =0;
    test.assignmentCounter = 0;

    /*****PART ONE:
     *****LOADING THE FILE INTO A BINOMIAL
     *****QUEUE LINKED WITH A HASHMAP
     */
    cout << "PART ONE: Loading into Priority Queue" << endl;
    while (openfile.good())
    {
        getline(openfile, line);
        //Increment line number
        lineNumber++;
        stringstream ss;
        ss << line;
        while(ss)
        {
            string word;
            ss >> word;
            if(ss)
                test.insert(word, lineNumber);
        }
    }
    cout << endl;

    /*****PART TWO:
     *****OUTPUT THE NUMBER OF COMPARISONS
     *****AND ASSIGNMENTS TO INSERT N ITEMS
     */
    cout << "PART TWO: Counters" << endl;
    cout << test.comparisonCounter << " comparisons and " << test.assignmentCounter;
    cout << " assignments were executed for the insertion of " << test.currentSize;
    cout << " elements." << endl << endl;

    /*****PART THREE:
     *****TESTS deleteMin() BY DELETING 10
     *****ITEMS AND OUTPUTTING KEY & LINE
     */
    cout << "PART THREE: Delete minimum 10 times" << endl;
    if (test.currentSize >= 10)
        for(unsigned int i = 0; i < 10; i++)
            test.deleteMin();
    else
        cout << "Cannot delete 10 items because there are currently less than 10 items." << endl;
    cout << endl;

    /*****PART FOUR:
     *****TESTS find(KEY) FUNCTION BY LOOKING UP
     *****A USER INPUTTED KEY AND OUTPUTTING THE LINE
     */
    cout << "PART FOUR: Test find(key)" << endl;
    string userInput;
    cout << "Search for word: ";
    cin >> userInput;
    if(test.find(userInput))
    {
        cout << "Search was successful." << endl;
        cout << userInput << " was found on line(s)";
        vector<int> temp = test.find(userInput)->lineNumber;
        for (unsigned int i = 0; i < temp.size(); i++)
            cout << " " << temp[i];
        cout << endl;
    }
    else
        cout << "Search was unsuccessful. Word does not exist." << endl;
    cout << endl;

    /*****PART FIVE:
     *****TESTS remove(key) BY ASKING THE USER FOR A WORD,
     *****REMOVING IT, THEN PERFORMING FIND TO SEE IF IT EXISTS.
     */
    cout << "PART FIVE: Test remove(key)" << endl;
    for (unsigned int i = 0; i < 5; i++)
    {
        cout << "Input word to remove: ";
        cin >> userInput;
        if (!test.isEmpty())
            test.remove(userInput);
        if(test.find(userInput))
        {
            cout << "Removal was unsuccessful." << endl;
        }
        else
            cout << "Removal was successful. " << userInput<< " does not exist." << endl;
    }
    cout << endl;

    /*****PART SIX:
     *****insert( x, y ) ALREADY INSERTS BY CREATING A NEW
     *****TREE OF SIZE 1 AND MERGING IT INTO THE OLD TREE
     */

    return 0;
}
