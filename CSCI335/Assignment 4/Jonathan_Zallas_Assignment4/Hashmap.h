//File:         Hashmap.h
//Author(s):    Jonathan Zallas
//Created on:   October 25, 2012
//Description:  Templated Hash Map
//Usage:        Fully functional Templated Hash map which uses
//              quadratic probing to deal with clustering.
//Version:      1.0
//
#ifndef HASHMAP_H_INCLUDED
#define HASHMAP_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>
using namespace std;

int nextPrime( int n );

// QuadraticProbing Hash Map class
//
// CONSTRUCTION: an approximate initial size or default of 101
//
// ******************PUBLIC OPERATIONS*********************
// bool insert( x )       --> Insert x
// bool remove( x )       --> Remove x
// bool contains( x )     --> Return true if x is present
// void makeEmpty( )      --> Remove all items
// int hash( string str ) --> Global method to hash strings

template <typename KeyType, typename ValueType>
class HashMap
{
  public:
    HashMap( int size = 101 ) : array( nextPrime( size ) )
    {
        makeEmpty( );
    }

    bool contains( const KeyType & x ) const
    {
        return isActive( findPos( x ) );
    }

    void makeEmpty( )
    {
        currentSize = 0;
        for( unsigned int i = 0; i < array.size( ); i++ )
        {
            array[ i ].info = EMPTY;
        }
    }

    bool insert( const KeyType & x, const ValueType & y )
    {
            // Insert x as active
        int currentPos = findPos( x );

        if( isActive( currentPos ) )
        {
            return false;
        }

        array[ currentPos ] = HashEntry( x, y, ACTIVE );

            // Rehash; see Section 5.5{}
        if( ++currentSize > array.size( ) / 2 )
        {
            rehash( );
        }

        return true;
    }

    ValueType *insert( const KeyType & x)
    {
        if( currentSize++ > array.size( ) / 2 )
        {
            rehash( );
        }

        int currentPos = findPos( x );

        if (array[currentPos].element == x && isActive(currentPos))
        {
            return &(array[currentPos].value);
        }

        array[currentPos].element = x;
        array[currentPos].info=ACTIVE;
        return &(array[currentPos].value);

    }

    bool remove( const KeyType & x )
    {
        int currentPos = findPos( x );
        if( !isActive( currentPos ) )
            return false;

        array[ currentPos ].info = DELETED;
        //Decrement currentSize
        currentSize--;
        return true;
    }

    enum EntryType { ACTIVE, EMPTY, DELETED };

    struct HashEntry
    {
        KeyType element;
        ValueType value;
        EntryType info;

        HashEntry( const KeyType & e = KeyType( ), const ValueType & v = ValueType( ), EntryType i = EMPTY )
        : element( e ), value( v ), info( i ) { }
    };

    vector<HashEntry> array;
    unsigned int currentSize;

    bool isActive( int currentPos ) const
      { return array[ currentPos ].info == ACTIVE; }

    int findPos( const KeyType & x ) const
    {

        int offset = 1;
        unsigned int currentPos = myhash( x );

        while( array[ currentPos ].info != EMPTY && array[ currentPos ].element != x )
        {
            currentPos += offset;  // Compute ith probe
            offset += 2;

            if( currentPos >= array.size( ) )
            {
                currentPos -= array.size( );
            }
        }

        return currentPos;
    }

    int findElement(const KeyType & x)
    {
        int index = findPos(x);
        if (array[index].info == ACTIVE && x == array[index].element)
        {
            return index;
        }
        return (-1);
    }

    void rehash( )
    {
        vector<HashEntry> oldArray = array;

        // Create new double-sized, empty table
        array.resize( nextPrime( 2 * oldArray.size( ) ) );
        for( unsigned int j = 0; j < array.size( ); j++ )
        {
            array[ j ].info = EMPTY;
        }
        // Copy table over
        currentSize = 0;
        for( unsigned int i = 0; i < oldArray.size( ); i++ )
        {
            if( oldArray[ i ].info == ACTIVE )
            {
                insert( oldArray[ i ].element, oldArray[i].value);
            }
        }
    }

    int myhash( const KeyType & x ) const
    {
        int hashVal = hash( x );

        hashVal %= array.size( );
        if( hashVal < 0 )
        {
            hashVal += array.size( );
        }

        return hashVal;
    }
};

int hash( const string & key );
int hash( int key );

#endif // HASHMAP_H_INCLUDED
