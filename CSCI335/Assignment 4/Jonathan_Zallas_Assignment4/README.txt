Assignment 4
Jonathan Zallas (jzallas@hunter.cuny.edu)

--Priority Queue
Heavily edited Priority Queue based off of the sources BinomialQueue.h, dsexceptions.h, QuadraticProbing.h and QuadraticProbing.cpp from Data Structures and Algorithm Analysis in C++ (3rd Edition) Mark Allen Weiss.

Every part of the assignment was completed. It was created to run with wordsHW2.txt


--To compile just type

         make all


--To run the program type
     
	 testPriorityQueue


--To remove object files type

         make clean
