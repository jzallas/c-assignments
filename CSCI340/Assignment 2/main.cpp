//File:         main.cpp
//Author(s):    Jonathan Zallas
//Created on:   April 14, 2013
//Description:  Operating Systems Data Structure and Scheduling Simulation
//Version:      2.1
//

#include <iostream>
#include <string>
#include <queue>
#include <vector>
#include <ctype.h>
#include <cstdlib>
#include <iomanip>

using namespace std;

void clear()
{
    //cout << string( 100, '\n' );
}

void ScreenPause()
{
    cout << "Press ENTER to continue...";
    cin.ignore();
    cin.get();
}

class PCB //process control block
{
public:
    int PID;
    string filename;
    int startLocation;
    bool read, write;
    int fileLength;
    float tau; //estimated burst
    float remainingTau; //estimated remaining burst
    float averageBurst; //average burst
    int numberOfBursts; //number of bursts
    float time; //total time spent in cpu
    int cylinder;
    PCB(float initialTau)
    {
        read = write = false;
        PID = startLocation = fileLength = -1;
        filename = "N/A";
        remainingTau = tau = initialTau;
        time = 0;
        cylinder = -1;
        averageBurst = 0;
        numberOfBursts = 0;
    }

    bool isNum(string & num)
    {
        //MAKE SURE NUMBER IS ENTERED
        for (unsigned int i = 0; i < num.length(); i++)
        {
            if (isalpha(num[i]))
                return false;
        }
        return true;
    }

    void setParameters()
    {
        string temp;
        cout << "Parameters for PID" << PID << endl << "Filename: ";
        cin >> filename;

        do
        {
            cout << "Starting location: ";
            cin >> temp;
        }
        while(!isNum(temp));
        startLocation = atoi(temp.c_str());

        do
        {
            cout << "Read or Write? (R/W): ";
            cin >> temp;
        }
        while(!(temp == "W" || temp == "w" ||temp == "R" || temp == "r"));

        if (temp == "W" || temp == "w")
            write = true;
        if (temp == "W" || temp == "w" || temp == "R" || temp == "r")
            read = true;

        if(write == true)
        {
            do
            {
                cout << "File Length: ";
                cin >> temp;
            }
            while(!isNum(temp));
            fileLength = atoi(temp.c_str());
        }
    }

    void setParametersDisk(const int & cylinders)
    {
        string temp;
        cout << "Parameters for PID" << PID << endl << "Filename: ";
        cin >> filename;

        do
        {
            cout << "Starting location: ";
            cin >> temp;
        }
        while(!isNum(temp));
        startLocation = atoi(temp.c_str());

        do
        {
            cout << "Read or Write? (R/W): ";
            cin >> temp;
        }
        while(!(temp == "W" || temp == "w" || temp == "R" || temp == "r"));

        if (temp == "W" || temp == "w")
            write = true;
        if (temp == "W" || temp == "w" || temp == "R" || temp == "r")
            read = true;

        if(write == true)
        {
            do
            {
                cout << "File Length: ";
                cin >> temp;
            }
            while(!isNum(temp));
            fileLength = atoi(temp.c_str());
        }

        do
        {
            do
            {
                cout << "Cylinder: ";
                cin >> temp;
            }
            while(!isNum(temp));
            cylinder = atoi(temp.c_str());
            if (cylinders < cylinder)
                cout << "This Hard Disk only has " << cylinders << " cylinder(s)." << endl;
        }
        while (cylinders < cylinder);
    }

    void setParametersRW()
    {
        string temp;
        cout << "Parameters for PID" << PID << endl << "Filename: ";
        cin >> filename;

        do
        {
            cout << "Starting location: ";
            cin >> temp;
        }
        while(!isNum(temp));
        startLocation = atoi(temp.c_str());

        read = true;
        write = true;

        do
        {
            cout << "File Length: ";
            cin >> temp;
        }
        while(!isNum(temp));
        fileLength = atoi(temp.c_str());
    }

    void updateTime()
    {
        float elapsedTime;
        string userInput;
        bool valid = true;
        do
        {
            valid = true;
            cout << "Elapsed CPU time(ms) : ";
            cin >> userInput;
            for (unsigned int i = 0; i < userInput.size(); i++)
            {
                if (isalpha(userInput[i]))
                    valid = false;
            }
        }
        while(!valid);
        elapsedTime = atof(userInput.c_str());

        remainingTau = remainingTau - elapsedTime;
        time = time + elapsedTime;
    }

    ///TOTAL TIME FOR THIS BURST IS (tau - remainingTau)

    void endBurst(const float & weight)
    {
        //update average burst for this process
        numberOfBursts++;
        if (numberOfBursts > 1 )
        {
            averageBurst = ((tau-remainingTau) + ((numberOfBursts-1)*averageBurst))/(numberOfBursts);
        }
        else
            averageBurst = (tau-remainingTau);

        //update tau next
        calculate_Tnext(weight);
        remainingTau = tau;
    }

    void calculate_Tnext(const float & weight) //next estimated burst
    {
        //updateTotalTime
        tau = (weight*tau) + ((1-weight)*(tau-remainingTau));
    }
};

class CompareTau
{
public:
    bool operator()(PCB* p1, PCB* p2) // p2 has higher priority than p1 if p2 tau is smaller than p1
    {
        if (p2->remainingTau < p1->remainingTau)
            return true;
        if (p2->remainingTau == p1->remainingTau) //use PID as tiebreaker
        {
            if (p2->PID < p1->PID)
                return true;
        }
        return false;
    }
};

class CompareCylinder
{
public:
    bool operator()(PCB* p1, PCB* p2) // p2 has higher priority than p1 if p2 cylinder is smaller than p1
    {
        if (p2->cylinder < p1->cylinder)
            return true;
        return false;
    }
};

class OperatingSystem
{
public:
    priority_queue<PCB*, vector<PCB*>, CompareTau> ready;

    vector<PCB*> ProcessList; //process list

    ///DEVICES
    vector<queue<PCB*> > printer; //PRINTERS

    ///vector<queue<PCB*> > disk;
    //Vector of Disks
    //Each Disk has a vector of priority queues, each dedicated to one process
    vector<vector<priority_queue<PCB*, vector<PCB*>, CompareCylinder>*> > disk2;
    vector<unsigned int> cylinders; // number of cylinders per disk
    vector<int> RoundRobinCounter;
    unsigned int timer;

    vector<queue<PCB*> > cdrw; //CD/RW

    PCB* CPU; //CPU

    float initialTau; //initial burst estimate

    float weight; // alpha

    int completed; // completed processes

    float systemAvg;

    OperatingSystem() //CONSTRUCTOR: ALSO SYSGEN
    {
        CPU = NULL;
        completed = 0;
        systemAvg = 0;
        timer = 0;

        string userInput;
        bool valid = true;
        cout << "Specify how many devices of each type..." << endl;
        do
        {
            valid = true;
            cout << "Printers : ";
            cin >> userInput;
            for (unsigned int i = 0; i < userInput.size(); i++)
            {
                if (isalpha(userInput[i]))
                    valid = false;
            }
        }
        while(!valid);
        printer.resize(atoi(userInput.c_str()));

        do
        {
            valid = true;
            cout << "Disks : ";
            cin >> userInput;
            for (unsigned int i = 0; i < userInput.size(); i++)
            {
                if (isalpha(userInput[i]))
                    valid = false;
            }
        }
        while(!valid);
        disk2.resize(atoi(userInput.c_str()));
        cylinders.resize(atoi(userInput.c_str()));
        RoundRobinCounter.resize(atoi(userInput.c_str()));

        do
        {
            valid = true;
            cout << "CD/RW : ";
            cin >> userInput;
            for (unsigned int i = 0; i < userInput.size(); i++)
            {
                if (isalpha(userInput[i]))
                    valid = false;
            }
        }
        while(!valid);
        cdrw.resize(atoi(userInput.c_str()));

        do
        {
            valid = true;
            cout << "Initial Burst Estimate (in Milliseconds) : ";
            cin >> userInput;
            for (unsigned int i = 0; i < userInput.size(); i++)
            {
                if (isalpha(userInput[i]))
                    valid = false;
            }
        }
        while(!valid);
        initialTau = atof(userInput.c_str());


        cout << "Number of cylinders" << endl;
        for (unsigned int j = 0; j < cylinders.size(); j++)
        {
            do
            {
                valid = true;
                cout << "\t Disk "<< j << " : ";
                cin >> userInput;
                for (unsigned int i = 0; i < userInput.size(); i++)
                {
                    if (isalpha(userInput[i]))
                        valid = false;
                }
            }
            while(!valid);
            cylinders[j] = atoi(userInput.c_str());
        }

        do
        {
            do
            {
                valid = true;
                cout << "Relative Weight alpha (0<=a<=1) : ";
                cin >> userInput;
                for (unsigned int i = 0; i < userInput.size(); i++)
                {
                    if (isalpha(userInput[i]))
                        valid = false;
                }
            }
            while(!valid);
            weight = atof(userInput.c_str());
        }
        while(!((0 <= weight) && (weight <= 1)));


    }

    ~OperatingSystem()
    {
        //erase all pending processes
        for (unsigned int i = 0; i < ProcessList.size(); i++)
        {
            if (ProcessList[i] != NULL)
            {
                delete ProcessList[i];
                ProcessList[i] = NULL;
            }
        }
    }

    void getSignal() //gets command
    {
        string signal;
        cin >> signal;
        if(validInput(signal))
        {
            if (isupper(signal[0]))
                Interrupt(signal);
            else
                SysCall(signal);
        }
    }

    bool SysCall(string & input) //syscall if process is in cpu
    {
        if (CPU != NULL)
        {
            //"t" termination of process
            if (input[0] == 't' && input.length() == 1)
            {
                ejectCPU(true);
                if(!killProcess()) //if cannot kill cpu process
                    return false;

                return true;
            }

            ///device calls

            //"p" printer
            if (input[0] == 'p'&& input.length() > 1)
            {

                unsigned int temp = atoi(input.substr(1).c_str());
                if (temp < printer.size())
                {
                    ejectCPU(true);
                    CPU->setParametersRW();
                    (printer[temp]).push(CPU);
                    CPU = NULL;
                    return true;
                }
            }

            //"d" disk
            if (input[0] == 'd'&& input.length() > 1)
            {
                unsigned int temp = atoi(input.substr(1).c_str());
                if (temp < disk2.size())
                {
                    ejectCPU(true);
                    CPU->setParametersDisk(cylinders[temp]);
                    //(disk[temp]).push(CPU);
                    CFQinsert(CPU, temp);
                    CPU = NULL;
                    return true;
                }
            }

            //"c" cdrw
            if (input[0] == 'c'&& input.length() > 1)
            {
                unsigned int temp = atoi(input.substr(1).c_str());
                if (temp < cdrw.size())
                {
                    ejectCPU(true);
                    CPU->setParameters();
                    (cdrw[temp]).push(CPU);
                    CPU = NULL;
                    return true;

                }
            }
        }
        return false;
    }

    void updateAvg(float newTime) // updates system average
    {
        if (completed != 0)
        {
            systemAvg = (newTime + ((completed-1)*systemAvg))/(completed);
        }
        else
            systemAvg = newTime;
    }

    bool validInput(string & input) //checks if command is in valid format
    {
        if (isalpha(input[0])) //if the first character is a letter
        {
            if(input.length() > 1) //if its bigger than 1 char command
            {
                for (unsigned int i = 1; i <= (input.length() -1); i++) //check every character after
                {
                    if (!isdigit(input[i])) //if every character after isn't a number, reject
                        return false;
                }
            }
            return true;
        }
        return false;
    }

    void displayReady() //displays snapshot for ready
    {
        cout << "\tSystem Average : ";
        if (completed == 0)
            cout << "N/A" << endl;
        else
            cout << systemAvg << " milliseconds" << endl;
        cout << left << setw(6) << "PID" << setw(18)<< "Total CPU Time" << setw(18) << "Avg Burst Time" << setw(19) << "Estimated Burst"
            << setw(17) << "Remaining" << endl << endl;
        unsigned int counter = 0;
        queue<PCB*> temp;
        while (!ready.empty())
        {
            cout << setw(6) << ready.top()->PID << setw(18) << ready.top()->time;

            if (ready.top()->numberOfBursts == 0)
                cout << left << setw(18) << "N/A";
            else
                cout << left << setw(18) << ready.top()->averageBurst;
                cout << left << setw(19) << ready.top()->tau << setw(17) << ready.top()->remainingTau << endl;

            temp.push(ready.top()); //put it in the temp queue
            ready.pop(); // remove from front of queue

            counter++;
            if (counter == 19)
            {
                counter = 0;
                ScreenPause();
                cout << left << setw(6) << "PID" << setw(18)<< "Total CPU Time" << setw(18) << "Avg Burst Time" << setw(19) << "Estimated Burst"
                    << setw(17) << "Remaining" << endl << endl;
            }
        }
        while (!temp.empty()) //put everything back in priority queue
        {
            ready.push(temp.front());
            temp.pop();
        }
    }

    void displayQueueDisk() //displays snapshot for disk
    {
        cout << "\tSystem Average : ";
        if (completed == 0)
            cout << "N/A" << endl;
        else
            cout << systemAvg << " milliseconds" << endl;
        cout << left << setw(5) << "PID" << setw(6) << "Disk#"
             << setw(10) << "Cylinder" << setw(13)<< "Filename" << setw(10) << "StartLoc"
             << setw(9) << "FileLen" << setw(5) << "R/W"
             << setw(9) << "TotalCPU" << setw(9) << "AvgBurst" << endl << endl;
        unsigned int counter = 0;
        //go through each disk i
        for (unsigned int i = 0; i < disk2.size(); i++)
        {
            queue <PCB*> temp;
            //go through each process j queue on the disk
            for (unsigned int j = 0; j < disk2[i].size(); j++)
            {
                //
                if(disk2[i][j] != NULL)
                {
                    while (!disk2[i][j]->empty())
                    {
                        cout << setw(5) << disk2[i][j]->top()->PID << setw(6) << i << setw(10) << disk2[i][j]->top()->cylinder << setw(13) << disk2[i][j]->top()->filename
                             << setw(10) << disk2[i][j]->top()->startLocation;
                        if (disk2[i][j]->top()->fileLength != -1)
                            cout << setw(9) << disk2[i][j]->top()->fileLength;
                        else
                            cout << setw(9) << "N/A";
                        if (disk2[i][j]->top()->read == true)
                            cout << "R/";
                        else
                            cout << " /";
                        if (disk2[i][j]->top()->write == true)
                            cout << setw(3) << "W";
                        else
                            cout << setw(3) <<" ";
                        cout << setw(9) << disk2[i][j]->top()->time << setw(9) << disk2[i][j]->top()->averageBurst << endl;


                        //pop everything temporarily
                        temp.push(disk2[i][j]->top());
                        disk2[i][j]->pop();

                        counter++;
                        if (counter == 19)
                        {
                            counter = 0;
                            ScreenPause();
                            cout << left << setw(5) << "PID" << setw(6) << "Disk#"
                                << setw(10) << "Cylinder" << setw(13)<< "Filename" << setw(10) << "StartLoc"
                                << setw(9) << "FileLen" << setw(5) << "R/W"
                                << setw(9) << "TotalCPU" << setw(9) << "AvgBurst" << endl << endl;
                        }
                    }
                }
                //put everything back
                while(!temp.empty())
                {
                    disk2[i][j]->push(temp.front());
                    temp.pop();
                }

            }
        }
    }

    void displayQueue(vector<queue<PCB*> > & deviceQueue) //displays for snapshot interrupt
    {
        cout << "\tSystem Average : ";
        if (completed == 0)
            cout << "N/A" << endl;
        else
            cout << systemAvg << " milliseconds" << endl;
        cout << left << setw(5) << "PID";
        if (deviceQueue == printer)
            cout << setw(9) << "Printer#";
        else if (deviceQueue == cdrw)
            cout << setw(9) << "CDRW#";
        cout << setw(13)<< "Filename" << setw(10) << "StartLoc"
             << setw(9) << "FileLen" << setw(5) << "R/W"
             << setw(9) << "TotalCPU" << setw(9) << "AvgBurst" << endl << endl;
        unsigned int counter = 0;
        for (unsigned int i = 0; i < deviceQueue.size(); i++)
        {
            for (unsigned int j = 0; j < deviceQueue[i].size(); j++)
            {
                //recycle each PCB and display
                cout << setw(5) << deviceQueue[i].front()->PID << setw(9) << i << setw(13) << deviceQueue[i].front()->filename
                     << setw(10) << deviceQueue[i].front()->startLocation;
                if (deviceQueue[i].front()->fileLength != -1)
                    cout << setw(9) << deviceQueue[i].front()->fileLength;
                else
                    cout << setw(9) << "N/A";
                if (deviceQueue[i].front()->read == true)
                    cout << "R/";
                else
                    cout << " /";
                if (deviceQueue[i].front()->write == true)
                    cout << setw(3) << "W";
                else
                    cout << setw(3) <<" ";
                cout << setw(9) << deviceQueue[i].front()->time << setw(9) << deviceQueue[i].front()->averageBurst << endl;

                deviceQueue[i].push(deviceQueue[i].front()); //put it at back of queue
                deviceQueue[i].pop(); // remove from front of queue
                counter++;
                if (counter == 19)
                {
                    counter = 0;
                    ScreenPause();
                    cout << left << setw(5) << "PID";
                    if (deviceQueue == printer)
                        cout << setw(9) << "Printer#";
                    else if (deviceQueue == cdrw)
                        cout << setw(9) << "CDRW#";
                    cout << setw(13)<< "Filename" << setw(10) << "StartLoc"
                         << setw(9) << "FileLen" << setw(5) << "R/W"
                         << setw(9) << "TotalCPU" << setw(9) << "AvgBurst" << endl << endl;
                }

            }
        }
    }

    void ejectCPU(bool end) //preps to kick process out of cpu and update time
    {
        if (CPU != NULL)
        {
            CPU->updateTime();

            if (end == false) //if its not the end of the burst
            {
                //put back in ready queue
                ready.push(CPU);
                CPU = NULL;
            }
            else //if it is the end of the burst
                CPU->endBurst(weight);
        }
    }

    bool Interrupt(string & input) //interrupt if process is in queue
    {
        //"A" arrival of new process
        if (input[0] == 'A' && input.length() == 1)
        {
            ejectCPU(false);
            if(createProcess()) //if can create process
                return true;
            return false;
        }

        //"S" snapshot
        if (input[0] == 'S' && input.length() == 1)
        {
            ejectCPU(false);
            string ssInput;
            do
            {
                cout << "Display which queue? {r/c/d/p} ";
                cin >> ssInput;
                if (ssInput == "r")
                {
                    clear();
                    cout << endl << "**********************************Ready Queue***********************************" << endl;
                    displayReady();
                    return true;
                }
                else if (ssInput == "p")
                {
                    clear();
                    cout << endl << "*********************************Print Queue(s)*********************************" << endl;
                    displayQueue(printer);
                    return true;
                }
                else if (ssInput == "d")
                {
                    clear();
                    cout << endl << "*********************************Disk Queue(s)**********************************" << endl;
                    displayQueueDisk();
                    return true;
                }
                else if (ssInput == "c")
                {
                    clear();
                    cout << endl << "*********************************CD/RW Queue(s)*********************************" << endl;
                    displayQueue(cdrw);
                    return true;
                }
            }
            while(!(ssInput == "c" || ssInput == "p" || ssInput == "d" || ssInput == "r"));
            return false;
        }

        ///device interrupts

        //"P" printer
        if (input[0] == 'P'&& input.length() > 1)
        {
            unsigned int temp = atoi(input.substr(1).c_str());
            if (temp < printer.size() && !printer[temp].empty())
            {
                ready.push((printer[temp]).front());
                (printer[temp]).pop();
                ejectCPU(false); // eject cpu process if interrupt was created
                return true;
            }
        }

        //"D" disk
        if (input[0] == 'D'&& input.length() > 1)
        {
            unsigned int temp = atoi(input.substr(1).c_str());
            if (temp < disk2.size())
            {
                if (CFQselect(temp))
                {
                    ejectCPU(false);
                    return true;
                }
            }
            /*if (temp < disk2.size() && !disk2[temp].empty())
            {
                ready.push((disk[temp]).front());
                (disk[temp]).pop();
                ejectCPU(); // eject cpu process if interrupt was created
                return true;
            }*/
        }

        //"C" cdrw
        if (input[0] == 'C'&& input.length() > 1)
        {
            unsigned int temp = atoi(input.substr(1).c_str());
            if (temp < cdrw.size() && !cdrw[temp].empty())
            {
                ready.push((cdrw[temp]).front());
                (cdrw[temp]).pop();
                ejectCPU(false); // eject cpu process if interrupt was created
                return true;
            }
        }

        return false;
    }

    bool killProcess() //kills current process in cpu
    {
        if (CPU != NULL)
        {
            completed++;
            updateAvg(CPU->time);
            cout << endl << left << setw(16) << "Terminated PID" << setw(23)<< "Total CPU Time(ms)" << setw(23)<< "System Average Total CPU(ms)" << endl << endl;
            cout << left << setw(16) <<  CPU->PID << setw(23) << CPU->time << setw(23) << systemAvg << endl;



            ProcessList[CPU->PID] = NULL;
            delete CPU;
            CPU = NULL;
            return true;
        }
        return false;
    }

    bool createProcess() //creates new process and pushes to ready queue
    {
        PCB * newProcess = new PCB(initialTau); //create process
        newProcess->PID = ProcessList.size(); // assign PID
        ProcessList.push_back(newProcess); //put it in process list
        ready.push(newProcess); //put it in ready queue
        return true;
    }

    bool loadProcess() //loads process from ready queue into cpu if cpu is idle
    {
        if (CPU == NULL && !ready.empty())
        {
            CPU = ready.top();
            ready.pop();
            return true;
        }
        return false;
    }

//Completely fair Queueing - Give this algorithm a process and it organizes it for least seek time without starvation
//Since System clock is not emulated, each process gets 3(arbitrary number) rounds of anticipation before it moves onto next process.
//Also, since only one cylinder is requested per process, each process will have a pqueue with only 1 item in it

    //inserts a process into appropriate destination disk queue
    //  either finds an appropriate queue or
    //  creates an appropriate queue if it couldn't find a spot
    void CFQinsert(PCB* process, int destination)
    {
        //if the disk vector was big enough to deal with that process, check to see if its there
        if ((int)disk2[destination].size() > process->PID)
        {
            //if the process is already there
            if (disk2[destination][process->PID] != NULL)
            {
                disk2[destination][process->PID]->push(process);
                return;
            }
        }
        else //vector too small to have process
        {
            disk2[destination].resize((process->PID)+1);
        }
        //process wasnt there
        disk2[destination][process->PID] = new priority_queue<PCB*, vector<PCB*>, CompareCylinder>;
        disk2[destination][process->PID]->push(process);
    }

    //selects which process to complete from the selected disk of origin
    //  returns true if was able to complete and return a task to the ready queue
    //  returns false if nothing waiting or unable to return to ready queue
    bool CFQselect(int origin)
    {
        for (unsigned int i = 0; i < disk2[origin].size(); i++)
        {
            //ask round robin whos turn is it then
            //check if queue exists for that process
            if(disk2[origin][RoundRobinCounter[origin]] != NULL)
            {
                //if queue exists and is empty, its a failed previous anticipation so free memory and move along
                if (disk2[origin][RoundRobinCounter[origin]]->empty())
                {
                    delete disk2[origin][RoundRobinCounter[origin]];
                    disk2[origin][RoundRobinCounter[origin]] = NULL;
                    timer = 0;
                    RoundRobinCounter[origin] = (RoundRobinCounter[origin]+1)% disk2[origin].size();
                }
                else if (timer >=3) //time slice expired
                {
                    timer = 0;
                    RoundRobinCounter[origin] = (RoundRobinCounter[origin]+1)% disk2[origin].size();
                }
                else // queue exists and contains something, do task and return it to ready queue
                {

                    timer++; //time slice increments for this process
                    ready.push(disk2[origin][RoundRobinCounter[origin]]->top());
                    disk2[origin][RoundRobinCounter[origin]]->pop();
                    if (disk2[origin][RoundRobinCounter[origin]]->empty()) //if this leaves the queue empty, anticipate sequential access and wait
                        return true;
                    //otherwise, tell round robin to check the next queue for the next round
                    timer = 0;
                    RoundRobinCounter[origin] = (RoundRobinCounter[origin]+1)% disk2[origin].size();
                    return true;
                }
            }
            else //if the decided process doesnt exist, need to move to next process and try again
            {
                timer = 0;
                RoundRobinCounter[origin] = (RoundRobinCounter[origin]+1)% disk2[origin].size();
            }
        }
        return false;
    }

};

int main()
{
    cout << "Notes: Devices and PIDs are automatically labeled starting from 0 to N" << endl;
    OperatingSystem * simulation = new OperatingSystem;
    while (true)
    {
        simulation->loadProcess();
        simulation->getSignal();
    }

    delete simulation;
    simulation = NULL;

    return 0;
}
