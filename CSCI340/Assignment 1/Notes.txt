Your "S-r" displays info that is meaningless to a process in the ready queue.

Disk files are opened either for reading (with read permission only) or for writing (which gives read and write permission). It makes no sense to open a file for neither reading nor writing. Why would I open it?

Overall the program works well, doesn't crash, does what it should. The two issues above aren't worth anything, but you will probably want to fix the first in the next program. 10.