//File:         main.cpp
//Author(s):    Jonathan Zallas
//Created on:   March 10, 2013
//Description:  Operating Systems Data Structure Simulation
//Version:      1.0
//

#include <iostream>
#include <string>
#include <queue>
#include <vector>
#include <ctype.h>
#include <cstdlib>
#include <iomanip>

using namespace std;

void clear()
{
    //cout << string( 100, '\n' );
}

void ScreenPause()
{
    cout << "Press ENTER to continue...";
    cin.ignore();
    cin.get();
}

class PCB //process control block
{
public:
    int PID;
    string filename;
    int startLocation;
    bool read, write;
    int fileLength;
    PCB()
    {
        read = write = false;
        PID = startLocation = fileLength = -1;
        filename = "N/A";
    }

    bool isNum(string & num)
    {
        //MAKE SURE NUMBER IS ENTERED
        for (unsigned int i = 0; i < num.length(); i++)
        {
            if (isalpha(num[i]))
                return false;
        }
        return true;
    }

    void setParameters()
    {
        string temp;
        cout << "Parameters for PID" << PID << endl << "Filename: ";
        cin >> filename;

        do
        {
            cout << "Starting location: ";
            cin >> temp;
        }while(!isNum(temp));
        startLocation = atoi(temp.c_str());

        do
        {
            cout << "Read (Y/N): ";
            cin >> temp;
        }while(!(temp == "Y" || temp == "y" ||temp == "N" || temp == "n"));
        if (temp == "Y" || temp == "y")
            read = true;

        do
        {
            cout << "Write (Y/N): ";
            cin >> temp;
        }while(!(temp == "Y" || temp == "y" ||temp == "N" || temp == "n"));
        if (temp == "Y" || temp == "y")
            write = true;

        if(write == true)
        {
            do
            {
                cout << "File Length: ";
                cin >> temp;
            }while(!isNum(temp));
            fileLength = atoi(temp.c_str());
        }
    }

    void setParametersRW()
    {
        string temp;
        cout << "Parameters for PID" << PID << endl << "Filename: ";
        cin >> filename;

        do
        {
            cout << "Starting location: ";
            cin >> temp;
        }while(!isNum(temp));
        startLocation = atoi(temp.c_str());

        read = false;
        write = true;

        do
        {
            cout << "File Length: ";
            cin >> temp;
        }while(!isNum(temp));
        fileLength = atoi(temp.c_str());
    }
};

class OperatingSystem
{
public:
    queue<PCB*> ready; //ready queue

    vector<PCB*> ProcessList; //process list

    ///DEVICES
    vector<queue<PCB*> > printer; //PRINTERS

    vector<queue<PCB*> > disk; //DISK

    vector<queue<PCB*> > cdrw; //CD/RW

    PCB* CPU; //CPU



    OperatingSystem() //CONSTRUCTOR: ALSO SYSGEN
    {
        CPU = NULL;

        string deviceCounter;
        bool valid = true;
        cout << "Specify how many devices of each type..." << endl;
        do
        {
            valid = true;
            cout << "Printers : ";
            cin >> deviceCounter;
            for (unsigned int i = 0; i < deviceCounter.size(); i++)
            {
                if (isalpha(deviceCounter[i]))
                    valid = false;
            }
        }while(!valid);
        printer.resize(atoi(deviceCounter.c_str()));

        do
        {
            valid = true;
            cout << "Disks : ";
            cin >> deviceCounter;
            for (unsigned int i = 0; i < deviceCounter.size(); i++)
            {
                if (isalpha(deviceCounter[i]))
                    valid = false;
            }
        }while(!valid);
        disk.resize(atoi(deviceCounter.c_str()));

        do
        {
            valid = true;
            cout << "CD/RW : ";
            cin >> deviceCounter;
            for (unsigned int i = 0; i < deviceCounter.size(); i++)
            {
                if (isalpha(deviceCounter[i]))
                    valid = false;
            }
        }while(!valid);
        cdrw.resize(atoi(deviceCounter.c_str()));
    }

    ~OperatingSystem()
    {
        //erase all pending processes
        for (unsigned int i = 0; i < ProcessList.size(); i++)
        {
            if (ProcessList[i] != NULL)
            {
                delete ProcessList[i];
                ProcessList[i] = NULL;
            }
        }
    }

    void getSignal() //gets command
    {
        string signal;
        cin >> signal;
        if(validInput(signal))
        {
            if (isupper(signal[0]))
                Interrupt(signal);
            else
                SysCall(signal);
        }
    }

    bool SysCall(string & input) //syscall if process is in cpu
    {
        if (CPU != NULL)
        {
            //"t" termination of process
            if (input[0] == 't' && input.length() == 1)
            {
                if(!killProcess()) //if cannot kill cpu process
                    return false;

                return true;
            }

            ///device calls

            //"p" printer
            if (input[0] == 'p'&& input.length() > 1)
            {
                unsigned int temp = atoi(input.substr(1).c_str());
                if (temp < printer.size())
                {
                    CPU->setParametersRW();
                    (printer[temp]).push(CPU);
                    CPU = NULL;
                    return true;
                }
            }

            //"d" disk
            if (input[0] == 'd'&& input.length() > 1)
            {
                unsigned int temp = atoi(input.substr(1).c_str());
                if (temp < disk.size())
                {
                    CPU->setParameters();
                    (disk[temp]).push(CPU);
                    CPU = NULL;
                    return true;
                }
            }

            //"c" cdrw
            if (input[0] == 'c'&& input.length() > 1)
            {
                unsigned int temp = atoi(input.substr(1).c_str());
                if (temp < cdrw.size())
                {
                    CPU->setParameters();
                    (cdrw[temp]).push(CPU);
                    CPU = NULL;
                    return true;
                }
            }
        }
        return false;
    }

    bool validInput(string & input) //checks if command is in valid format
    {
        if (isalpha(input[0])) //if the first character is a letter
        {
            if(input.length() > 1) //if its bigger than 1 char command
            {
                for (unsigned int i = 1; i <= (input.length() -1); i++) //check every character after
                {
                    if (!isdigit(input[i])) //if every character after isn't a number, reject
                        return false;
                }
            }
            return true;
        }
        return false;
    }

    void displayReady()
    {
        cout << left << setw(16) << "PID" << setw(16)<< "Filename" << setw(16) << "Start Location" <<  setw(16) << "File Length" << setw(16) << "R/W" << endl;
        unsigned int counter = 0;
        for (unsigned int j = 0; j < ready.size(); j++)
            {
                cout << setw(16) <<  ready.front()->PID << setw(16) << ready.front()->filename;
                if (ready.front()->startLocation != -1)
                    cout << setw(16) << ready.front()->startLocation;
                else
                    cout << setw(16) << "N/A";
                if (ready.front()->fileLength != -1)
                    cout << setw(16) << ready.front()->fileLength;
                else
                    cout << setw(16) << "N/A";
                if (ready.front()->read == true)
                    cout << "Y/";
                else
                    cout << "N/";
                if (ready.front()->write == true)
                    cout << "Y\n";
                else
                    cout << "N\n";

                ready.push(ready.front()); //put it at back of queue
                ready.pop(); // remove from front of queue

                counter++;
                if (counter == 20)
                {
                    counter = 0;
                    ScreenPause();
                    cout << left << setw(16) << "PID" << setw(16)<< "Filename" << setw(16) << "Start Location" <<  setw(16) << "File Length" << setw(16) << "R/W" << endl;
                }
            }
    }

    void displayQueue(vector<queue<PCB*> > deviceQueue) //displays for snapshot interrupt
    {
        cout << left << setw(10) << "PID" << setw(10);
        if (deviceQueue == printer)
            cout << setw(10) << "Printer #";
        else if (deviceQueue == disk)
            cout << setw(10) << "Disk #";
        else if (deviceQueue == cdrw)
            cout << setw(10) << "CDRW #";
        cout << setw(15)<< "Filename" << setw(15) << "Start Location"
            << setw(15) << "File Length" << setw(15) << "R/W" << endl;
        unsigned int counter = 0;
        for (unsigned int i = 0; i < deviceQueue.size(); i++)
        {
            for (unsigned int j = 0; j < deviceQueue[i].size(); j++)
            {
                //recycle each PCB and display
                cout << setw(10) << deviceQueue[i].front()->PID << setw(10) << i << setw(15) << deviceQueue[i].front()->filename
                    << setw(15) << deviceQueue[i].front()->startLocation;
                if (deviceQueue[i].front()->fileLength != -1)
                    cout << setw(15) << deviceQueue[i].front()->fileLength;
                else
                    cout << setw(15) << "N/A";
                if (deviceQueue[i].front()->read == true)
                    cout << "Y/";
                else
                    cout << "N/";
                if (deviceQueue[i].front()->write == true)
                    cout << "Y\n";
                else
                    cout << "N\n";

                deviceQueue[i].push(deviceQueue[i].front()); //put it at back of queue
                deviceQueue[i].pop(); // remove from front of queue
                counter++;
                if (counter == 20)
                {
                    counter = 0;
                    ScreenPause();
                    cout << left << setw(10) << "PID" << setw(10);
                    if (deviceQueue == printer)
                        cout << setw(10) << "Printer #";
                    else if (deviceQueue == disk)
                        cout << setw(10) << "Disk #";
                    else if (deviceQueue == cdrw)
                        cout << setw(10) << "CDRW #";
                    cout << setw(15)<< "Filename" << setw(15) << "Start Location"
                        << setw(15) << "File Length" << setw(15) << "R/W" << endl;
                }

            }
        }
    }

    bool Interrupt(string & input) //interrupt if process is in queue
    {
        //"A" arrival of new process
        if (input[0] == 'A' && input.length() == 1)
            {
                if(createProcess()) //if cannot kill cpu process
                    return true;
            }

        //"S" snapshot
        if (input[0] == 'S' && input.length() == 1)
            {
                string ssInput;
                cin >> ssInput;
                if (ssInput == "r")
                {
                    clear();
                    cout << "Ready Queue" << endl;
                    displayReady();
                    return true;
                }
                else if (ssInput == "p")
                {
                    clear();
                    cout << "Print Queue(s)" << endl;
                    displayQueue(printer);
                    return true;
                }
                else if (ssInput == "d")
                {
                    clear();
                    cout << "Disk Queue(s)" << endl;
                    displayQueue(disk);
                    return true;
                }
                else if (ssInput == "c")
                {
                    clear();
                    cout << "CD/RW Queue(s)" << endl;
                    displayQueue(cdrw);
                    return true;
                }
                return false;
            }

        ///device interrupts

        //"P" printer
        if (input[0] == 'P'&& input.length() > 1)
            {
                unsigned int temp = atoi(input.substr(1).c_str());
                if (temp < printer.size() && !printer[temp].empty())
                {
                    ready.push((printer[temp]).front());
                    (printer[temp]).pop();
                    return true;
                }
            }

        //"D" disk
        if (input[0] == 'D'&& input.length() > 1)
            {
                unsigned int temp = atoi(input.substr(1).c_str());
                if (temp < disk.size() && !disk[temp].empty())
                {
                    ready.push((disk[temp]).front());
                    (disk[temp]).pop();
                    return true;
                }
            }

        //"C" cdrw
        if (input[0] == 'C'&& input.length() > 1)
            {
                unsigned int temp = atoi(input.substr(1).c_str());
                if (temp < cdrw.size() && !cdrw[temp].empty())
                {
                    ready.push((cdrw[temp]).front());
                    (cdrw[temp]).pop();
                    return true;
                }
            }

        return false;
    }

    bool killProcess() //kills current process in cpu
    {
        if (CPU != NULL)
        {
            ProcessList[CPU->PID] = NULL;
            delete CPU;
            CPU = NULL;
            return true;
        }
        return false;
    }

    bool createProcess() //creates new process and pushes to ready queue
    {
        PCB * newProcess = new PCB; //create process
        newProcess->PID = ProcessList.size(); // assign PID
        ProcessList.push_back(newProcess); //put it in process list
        ready.push(newProcess); //put it in ready queue
        return true;
    }

    bool loadProcess() //loads process from ready queue into cpu if cpu is idle
    {
        if (CPU == NULL && !ready.empty())
        {
            CPU = ready.front();
            ready.pop();
            return true;
        }
        return false;
    }

};

int main()
{
    cout << "Notes: Devices and PIDs are automatically labled starting from 0 to N" << endl;
    OperatingSystem * simulation = new OperatingSystem;
    while (true)
    {
        simulation->loadProcess();
        simulation->getSignal();
    }

    delete simulation;
    simulation = NULL;

	return 0;
}
