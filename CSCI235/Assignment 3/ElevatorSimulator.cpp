//File:         ElevatorSimulator.cpp
//Author(s):    Jonathan Zallas, Makina Celestine, Vishal Seecharran
//Created on:   October 30, 2011
//Description:  Program that simulates Elevators in a building with
//              user provided details.
//Usage:        Reads a *.prop file from command line in form
//              '<programname>.exe <filename>.prop'. The objective of
//              this simulation is to provide the number of
//              customers and the mean wait time for all the customers
//              generated in eight hours. If it is possible, the simulation
//              will also simulate the alternative with one less elevator
//              than what was provided in the *.prop file.
//Version:      1.0
//

#include "ElevatorSimulator.h"
void ReadProp::getPropData(const char* filename)
{
    source.open(filename);
    if (source.is_open())
    {
        string templine;
        string tempword;
        while(getline(source, templine))
        {
            istringstream getWord(templine);
            getWord >> tempword;
            if (tempword != "#" && tempword != "")
            {
                if(tempword == "elevators")
                getWord >> tempword >> elevators;
                else if(tempword == "floors")
                getWord >> tempword >> floors;
                else if(tempword == "weight_limit")
                getWord >> tempword >> weight_limit;
                else if(tempword == "min_weight")
                getWord >> tempword >> min_weight;
                else if(tempword == "max_weight")
                getWord >> tempword >> max_weight;
                else if(tempword == "customers")
                getWord >> tempword >> customer_rate;
            }
        }
        source.close();
    }
    else
    {
        cout << "Error opening file";
    }
}

void Customer::randomWD(const ReadProp& prop)
{
    weight = (rand() % (prop.max_weight+1-prop.min_weight)+ prop.min_weight);
    destinationFlr = (rand() % prop.floors)+1;
}

bool Elevator::enterCustomer(Customer& customer, const ReadProp& prop)  // returns false if cust can't enter elevator. Othwerwise puts cust in the appropriate table, adjusts weight, and returns true
{
    if ((totalWeight+customer.weight) >prop.weight_limit)
        return false;
    totalWeight = customer.weight + totalWeight;
    destination[customer.destinationFlr -1].push(customer);
    return true;

}

bool Elevator::exitCustomers() // removes everyone that wants to get off on currFloor
{
    bool peopleExited = false;
    while(destination[currentFloor-1].size()!= 0)
    {
        totalWeight = totalWeight - destination[currentFloor-1].front().weight;
        destination[currentFloor-1].pop();
        peopleExited = true;
    }
    return peopleExited;
}

int Elevator::numberOfRequests(int floorNumber = -1, STATE upOrDown = IDLE, STATE aboveOrBelow = IDLE) //returns number of pending requests above or below the floor number
{
    int numberOfRequests = 0;
    //by default, this just counts the number of requests in both up and down requests
    if (floorNumber == -1){
        for (unsigned int i = 0; i < upRequests.size(); i++){
            if (upRequests[i] == true)
                numberOfRequests++;
            if (dnRequests[i] == true)
                numberOfRequests++;
            }
    }
    else if (upOrDown == MOVING_UP) //if only counting the up queue
    {
        if (aboveOrBelow == MOVING_UP)  //start counting from index going up
        {
            for(unsigned int i = floorNumber+1; i <= upRequests.size(); i++){
                if (upRequests[i-1] == true)
                    numberOfRequests++;
            }
        }else if (aboveOrBelow == MOVING_DOWN){ //start counting from index going down
            for(int i = floorNumber-1; i >= 1; i--){
                if (upRequests[i-1] == true)
                    numberOfRequests++;
            }
        }
    } else if (upOrDown == MOVING_DOWN) //if only counting the dn queue
    {
        if (aboveOrBelow == MOVING_UP) //start counting from index going up
        {
            for(unsigned int i = floorNumber+1; i <= dnRequests.size(); i++){
                if (dnRequests[i-1] == true)
                    numberOfRequests++;
            }
        }else if (aboveOrBelow == MOVING_DOWN){  //start counting from index going down
            for(int i = floorNumber-1; i >= 1; i--){
                if (dnRequests[i-1] == true)
                    numberOfRequests++;
            }
        }
    }
        return(numberOfRequests);
}

bool Elevator::goUp()
{
    currentFloor++;
    if (currentFloor == (int)destination.size())
        elevator_state = MOVING_DOWN;
    return true;
}

bool Elevator::goDown()
{
    currentFloor--;
    if (currentFloor == 1)
        elevator_state = MOVING_UP;
    return true;
}

void Elevator::requestFromElevator(int destination) //when called, will update the vector of floors that that elevator needs to stop at
{
    if (destination > currentFloor){
        upRequests[destination-1] = true;
    } else if (destination < currentFloor){
        dnRequests[destination-1] = true;
        }
}

void Floor::generateCust(const ReadProp& prop, Elevator_Bank& bank, const unsigned int currentTime, WaitTime& wait)
{
    //everytime this is called, it will cycle through every floor and generate a customer based on the rate in the prop file.


    for(int i=1; i<= prop.floors; i++)
    {
        if(((rand()% 101)/100.00) <= prop.customer_rate)
        {
            Customer temp;
            wait.truetotal++;
            temp.randomWD(prop);
            temp.startTime = currentTime;
            while(temp.destinationFlr == i){
                temp.randomWD(prop);
            }
            if (temp.destinationFlr < i)
            {
                dnQueue[i-1].push(temp);
                bank.requestFromFloor(i, MOVING_DOWN, true);
            }
            else if (temp.destinationFlr > i)
            {
                upQueue[i-1].push(temp);
                bank.requestFromFloor(i, MOVING_UP, true);
            }
        }

    }
}

bool Floor::enterElevator(Elevator& elevator, const ReadProp& prop, Elevator_Bank& bank, WaitTime& wait, const unsigned int currentTime)//loads as much of the appropriate queue as possible onto the elevator when the it stops on that floor
{
    if(elevator.elevator_state == MOVING_UP) //if the elevator is going up
    {
        while(!upQueue[elevator.currentFloor-1].empty()){//check to see if there's anyone waiting to go up on the floor the elevator stopped at
            if(elevator.enterCustomer(upQueue[elevator.currentFloor-1].front(), prop)){//if there is someone waiting and they fit in the elevator, let them in
                wait.updateWait(upQueue[elevator.currentFloor-1].front().startTime, currentTime); //as they enter the elevator, calculate the wait time
                elevator.requestFromElevator(upQueue[elevator.currentFloor-1].front().destinationFlr); //after they enter, they push a button and request a floor
                upQueue[elevator.currentFloor-1].pop();//the copy of the customer remaining in the upQueue is now removed since the customer has successfully entered the elevator
            }
            else {  //if there is someone waiting and they can't fit in the elevator
                bank.requestFromFloor(elevator.currentFloor, MOVING_UP, false); //send a request to the bank for another elevator
                return false;
            }
        }
        return true;

    }
    else if (elevator.elevator_state == MOVING_DOWN) //if the elevator is going up
    {
        while(!dnQueue[elevator.currentFloor-1].empty()){ //check to see if there's anyone waiting to go down on the floor the elevator stopped at
            if(elevator.enterCustomer(dnQueue[elevator.currentFloor-1].front(), prop)){ //if there is someone waiting and they fit in the elevator, let them in
                wait.updateWait(dnQueue[elevator.currentFloor-1].front().startTime, currentTime); //as they enter the elevator, calculate the wait time
                elevator.requestFromElevator(dnQueue[elevator.currentFloor-1].front().destinationFlr); //after they enter, they push a button and request a floor
                dnQueue[elevator.currentFloor-1].pop(); //the copy of the customer remaining in the dnQueue is now removed since the customer has successfully entered the elevator
            }
            else { //if there is someone waiting and they can't fit in the elevator
                bank.requestFromFloor(elevator.currentFloor, MOVING_DOWN, false); //send a request to the bank for another elevator
                return false;
            }
        }
        return true;
    }
    return true;
}

void Elevator_Bank::requestFromFloor(int origin, STATE desiredDirection, bool firstAttempt) // when called, will call chooseBestElevator and then update the vector of the best elevator to stop at that floor.
{
    int bestElevator = chooseBestElevator(origin, desiredDirection, firstAttempt);
    if (desiredDirection == MOVING_UP){
        bank[bestElevator].upRequests[origin-1] = true;
    }else if (desiredDirection == MOVING_DOWN){
        bank[bestElevator].dnRequests[origin-1] = true;
    }
    if (bank[bestElevator].elevator_state == IDLE)
    {
        if (bank[bestElevator].currentFloor > origin)
        {
            bank[bestElevator].elevator_state = MOVING_DOWN;
        }else if (bank[bestElevator].currentFloor < origin)
        {
            bank[bestElevator].elevator_state = MOVING_UP;
        }else if(bank[bestElevator].currentFloor == origin)
        {
            bank[bestElevator].elevator_state = desiredDirection;
        }
    }
}

int Elevator_Bank::chooseBestElevator(int origin, STATE desiredDirection, bool firstAttempt)  //will check the elevators and decide which is the best to stop on the floor. if there is no best, it will default to the first elevator.
{
    int bestChoice = 0;

    //if this is the first request
    if (firstAttempt == true){
        //cycle through all the elevators and if there is an elevator already on that floor, return that elevator
        for (unsigned int i = 0; i < bank.size(); i++){
            if (bank[i].currentFloor == origin && (bank[i].elevator_state == desiredDirection || bank[i].elevator_state == IDLE)) //if elevator is on same floor and going in same direction or is idle
                return i;
        }
    }

    //otherwise, if this is a repeat request due to full elevator or there is not an elevator on the same floor
    //cycle through all the elevators and find the best IDLE choice if there is one
    for (unsigned int i = 0; i < bank.size(); i++){
        if (bank[i].elevator_state == IDLE) //if an elevator is IDLE
            {
            if (abs(bank[bestChoice].currentFloor - origin) > abs(bank[i].currentFloor - origin)) //check if the distance is shorter than best choice
                bestChoice = i;
            }
    }
    //return that IDLE choice if there is one
    if (bank[bestChoice].elevator_state == IDLE)
        return bestChoice;

    //cycle through all the elevators and find the best choice thats going in the same direction as the customer
    for (unsigned int i = 0; i < bank.size(); i++){
        if ((bank[i].elevator_state == desiredDirection) && (bank[i].currentFloor < origin) && (desiredDirection == MOVING_UP)) //if the elevator is going in the same direction(up) and is below the customer
        {
            if (abs(bank[bestChoice].currentFloor - origin) > abs(bank[i].currentFloor - origin)) //check if distance is shorter than best choice
                bestChoice = i;
        }
        else if ((bank[i].elevator_state == desiredDirection) && (bank[i].currentFloor > origin) &&(desiredDirection == MOVING_DOWN)) //if the elevator is going in the same direction(down) and is above the customer
        {
            if (abs(bank[bestChoice].currentFloor - origin) > abs(bank[i].currentFloor - origin)) //check if distance is shorter than best choice
                bestChoice = i;
        }
        else{
            if(bank[bestChoice].numberOfRequests() > bank[i].numberOfRequests())
                bestChoice = i;
        }
    }
    //return the best elevator thats going in the same direction as the customer and hasn't passed that floor yet
    //if there were no elevators that fit that criteria, return the elevator with the smallest number of pending requests
    return bestChoice;
}

void Simulator::cycleElevators(Elevator_Bank& bank, Floor& floor, const ReadProp& prop, WaitTime& wait, const unsigned int currentTime)
{
    for (unsigned int i = 0; i < bank.bank.size(); i++)
    {
        if (bank.bank[i].numberOfRequests() == 0 && bank.bank[i].elevator_state != IDLE && bank.bank[i].timer >= 10)
        {
                bank.bank[i].elevator_state = IDLE;
                bank.bank[i].busy = NO;
        }
        else if (bank.bank[i].busy == IGNORE) //this is occuring because the elevator had to ignore the previous request for some reason
        {
            if (bank.bank[i].elevator_state == MOVING_UP && bank.bank[i].timer >= 10){
                bank.bank[i].busy = NO;
                bank.bank[i].goUp();                                    //send the elevator up a floor
                if (bank.bank[i].upRequests[bank.bank[i].currentFloor-1] == true )
                {
                    bank.bank[i].timer =0;
                    bank.bank[i].exitCustomers();
                    bank.bank[i].upRequests[bank.bank[i].currentFloor-1] = false;                 //having already reached its objective floor, it clears it from its destination queue
                    floor.enterElevator(bank.bank[i], prop, bank, wait, currentTime);
                }
            }
            else if (bank.bank[i].elevator_state == MOVING_DOWN && bank.bank[i].timer >= 10){
                bank.bank[i].busy = NO;
                bank.bank[i].goDown();                                    //send the elevator down a floor
                if (bank.bank[i].dnRequests[bank.bank[i].currentFloor-1] == true )
                {
                    bank.bank[i].timer =0;
                    bank.bank[i].exitCustomers();
                    bank.bank[i].dnRequests[bank.bank[i].currentFloor-1] = false;                 //having already reached its objective floor, it clears it from its destination queue
                    floor.enterElevator(bank.bank[i], prop, bank, wait, currentTime);
                }
            }
        }
        else if (bank.bank[i].busy == UP) //if the elevator is busy going up to get customers who want to go down, it will prioritize this
        {
            bank.bank[i].goUp();
            bank.bank[i].busy = UP;
            if (bank.bank[i].numberOfRequests(bank.bank[i].currentFloor, MOVING_DOWN, MOVING_UP) == 0)
            {
                bank.bank[i].timer =0;
                bank.bank[i].elevator_state = MOVING_DOWN;
                bank.bank[i].busy = NO;
                bank.bank[i].exitCustomers();
                bank.bank[i].dnRequests[bank.bank[i].currentFloor-1] = false;                 //having already reached its objective floor, it clears it from its destination queue
                floor.enterElevator(bank.bank[i], prop, bank, wait, currentTime);

            }
        }
        else if (bank.bank[i].busy == DOWN) //if the elevator is busy going down to get customers who want to go up, it will prioritize this
        {
            bank.bank[i].goDown();
            bank.bank[i].busy = DOWN;
            if (bank.bank[i].numberOfRequests(bank.bank[i].currentFloor, MOVING_UP, MOVING_DOWN) == 0)
            {
                bank.bank[i].timer =0;
                bank.bank[i].elevator_state = MOVING_UP;
                bank.bank[i].busy = NO;
                bank.bank[i].exitCustomers();
                bank.bank[i].upRequests[bank.bank[i].currentFloor-1] = false;                 //having already reached its objective floor, it clears it from its destination queue
                floor.enterElevator(bank.bank[i], prop, bank, wait, currentTime);

            }
        }
        else if (bank.bank[i].busy == NO) //otherwise if the elevator is following normal instructions
        {
            if (bank.bank[i].elevator_state == MOVING_UP)  //if the elevator is going up
            {
                if (bank.bank[i].upRequests[bank.bank[i].currentFloor-1] == true) //if there is a request for the current floor just wait there
                {
                    bank.bank[i].exitCustomers(); //let off any customers
                    bank.bank[i].upRequests[bank.bank[i].currentFloor-1] = false;         //having already reached its objective floor, it clears it from its destination queue
                    if (!floor.enterElevator(bank.bank[i], prop, bank, wait, currentTime)) //take on any customer, if it returns a kickback, then...
                        {
                            bank.bank[i].busy = IGNORE; // ignores requests on this floor if it couldnt go up due of weight issues
                        }
                }
                else if (bank.bank[i].numberOfRequests(bank.bank[i].currentFloor, MOVING_UP, MOVING_UP) > 0 && bank.bank[i].timer >= 10) //if there are requests above this floor to go up
                {
                    bank.bank[i].goUp();                                    //send the elevator up a floor
                    if (bank.bank[i].upRequests[bank.bank[i].currentFloor-1] == true )
                    {
                        bank.bank[i].timer =0;
                        bank.bank[i].exitCustomers();
                        bank.bank[i].upRequests[bank.bank[i].currentFloor-1] = false;                 //having already reached its objective floor, it clears it from its destination queue
                        floor.enterElevator(bank.bank[i], prop, bank, wait, currentTime);
                    }
                }
                else if (bank.bank[i].numberOfRequests(bank.bank[i].currentFloor, MOVING_DOWN, MOVING_UP) > 0 && bank.bank[i].timer >= 10) //if there were no customers going up, if there are customers above this floor that want to go down
                {
                    bank.bank[i].busy = UP; //elevator is set to busy until it gets to the highest floor that a customer wants to go down from
                    bank.bank[i].goUp();
                    if (bank.bank[i].numberOfRequests(bank.bank[i].currentFloor, MOVING_DOWN, MOVING_UP) == 0) //if there are no more requests to go down above the current floor
                    {
                        bank.bank[i].timer =0;
                        bank.bank[i].elevator_state = MOVING_DOWN; //this is now a down elevator
                        bank.bank[i].busy = NO; //and its no longer busy
                        bank.bank[i].exitCustomers();
                        bank.bank[i].dnRequests[bank.bank[i].currentFloor-1] = false;                 //having already reached its objective floor, it clears it from its destination queue
                        floor.enterElevator(bank.bank[i], prop, bank, wait, currentTime);

                    }
                }

            }
            else if (bank.bank[i].elevator_state == MOVING_DOWN)  //if the elevator is going down
            {
                if (bank.bank[i].dnRequests[bank.bank[i].currentFloor-1] == true) //if there is a request for the current floor just wait there
                {
                    bank.bank[i].exitCustomers(); //let off any customers
                    bank.bank[i].dnRequests[bank.bank[i].currentFloor-1] = false;         //having already reached its objective floor, it clears it from its destination queue
                    if (!floor.enterElevator(bank.bank[i], prop, bank, wait, currentTime)) //take on any customer, if it returns a kickback, then...
                        {
                            bank.bank[i].busy = IGNORE; // ignores requests on this floor if it couldnt go down due of weight issues
                        }
                }
                else if (bank.bank[i].numberOfRequests(bank.bank[i].currentFloor, MOVING_DOWN, MOVING_DOWN) > 0 && bank.bank[i].timer >= 10) //if there are below above this floor to go down
                {
                    bank.bank[i].goDown();                                    //send the elevator down a floor
                    if (bank.bank[i].dnRequests[bank.bank[i].currentFloor-1] == true )
                    {
                        bank.bank[i].timer =0;
                        bank.bank[i].exitCustomers();
                        bank.bank[i].dnRequests[bank.bank[i].currentFloor-1] = false;                 //having already reached its objective floor, it clears it from its destination queue
                        floor.enterElevator(bank.bank[i], prop, bank, wait, currentTime);
                    }
                }
                else if (bank.bank[i].numberOfRequests(bank.bank[i].currentFloor, MOVING_UP, MOVING_DOWN) > 0 && bank.bank[i].timer >= 10) //if there are requests to go up below the current floor
                {
                    bank.bank[i].busy = DOWN; //elevator is set to busy until it gets to the lowest floor that a customer wants to go up from
                    bank.bank[i].goDown();
                    if (bank.bank[i].numberOfRequests(bank.bank[i].currentFloor, MOVING_UP, MOVING_DOWN) == 0) //if there are no more requests to go up below the current floor
                    {
                        bank.bank[i].timer =0;
                        bank.bank[i].elevator_state = MOVING_UP; //this is now an up elevator
                        bank.bank[i].busy = NO; //and its no longer busy
                        bank.bank[i].exitCustomers();
                        bank.bank[i].upRequests[bank.bank[i].currentFloor-1] = false;                 //having already reached its objective floor, it clears it from its destination queue
                        floor.enterElevator(bank.bank[i], prop, bank, wait, currentTime);

                    }
                }
            }
        }
        bank.bank[i].timer++;
    }
}

void Simulator::runSimulator(const ReadProp& prop)
{
    //create an elevator bank
    Elevator_Bank simBank(prop);
    //create the floors
    Floor simFloor(prop);
    //object to track the times the customers waited
    WaitTime wait;


    for (unsigned int i = 1; i <= simTime; i++){
        if ((i%10)==0){
           simFloor.generateCust(prop, simBank, i, wait);
        }
        cycleElevators(simBank, simFloor, prop, wait, i);
    }
    cout << "---------------------------------" << endl << "        Elevator Simulator       " << endl << "---------------------------------" << endl;
    cout <<  prop.elevators << " Elevators" << endl << prop.floors << " Floors" << endl << prop.weight_limit << " Elevator Weight Limit" << endl << prop.min_weight << " Minimum Weight for Customers" << endl << prop.max_weight << " Maximum Weight for Customers" << endl << prop.customer_rate << " Customer Rate per Minute" << endl;
    cout << "---------------------------------" << endl;
    cout << "The total number of customers that got on the elevators was: " << wait.totalCustomers << endl;
    cout << "The actual number of customers generated was: " << wait.truetotal << endl << endl;
    if (wait.totalCustomers != wait.truetotal){
        cout << "The reason these numbers are different is because the simulation only lasted for strictly eight hours and ";
        int customersStillWaiting = 0;
        for (int i = 0; i < prop.floors; i++){
        if ((int)simFloor.upQueue[i].size() > 0)
            customersStillWaiting = customersStillWaiting + (int)simFloor.upQueue[i].size();
        if ((int)simFloor.dnQueue[i].size() > 0)
            customersStillWaiting = customersStillWaiting + (int)simFloor.dnQueue[i].size();
        }
    cout << customersStillWaiting << " customers were generated in the last couple of minutes before an elevator could reach them" << endl << endl;
    }
    cout << "The total wait time of all customers combined was " << wait.totalWaitTime*6 << " seconds." << endl;
    cout << "The average wait time was " << wait.calculateAverage() << " seconds." << endl;
}

void WaitTime::updateWait(unsigned int startTime, unsigned int currentTime)
{
    totalWaitTime = totalWaitTime + (currentTime - startTime); //update totalcycles
    totalCustomers++;
}

double WaitTime::calculateAverage()
{
    if (totalCustomers == 0)
        return 0;
    else
        return((double)totalWaitTime*6/totalCustomers); //in seconds
}
