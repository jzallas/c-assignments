//File:         ElevatorSimulator.h
//Author(s):    Jonathan Zallas, Makina Celestine, Vishal Seecharran
//Created on:   October 30, 2011
//Description:  Program that simulates Elevators in a building with
//              user provided details.
//Usage:        Reads a *.prop file from command line in form
//              '<programname>.exe <filename>.prop'. The objective of
//              this simulation is to provide the number of
//              customers and the mean wait time for all the customers
//              generated in eight hours. If it is possible, the simulation
//              will also simulate the alternative with one less elevator
//              than what was provided in the *.prop file.
//Version:      1.0
//


#ifndef ElevatorSimulator_h
#define ElevatorSimulator_h

#include <iostream>
#include <vector>
#include <queue>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <sstream>


using namespace std;

enum STATE {IDLE, MOVING_UP, MOVING_DOWN};
enum BUSY {UP, DOWN, NO, IGNORE};

class ReadProp
{
public:
    ifstream source;
    string temp;
    int elevators;
    int floors;
    int weight_limit;
    int min_weight;
    int max_weight;
    double customer_rate;

    void getPropData(const char* filename);

    ReadProp(const char* filename)
    {
        getPropData(filename);
    }
};

class WaitTime
{
    public:
    unsigned int totalWaitTime;
    unsigned int totalCustomers;
    unsigned int truetotal;
    void updateWait(unsigned int startTime, unsigned int currentTime);
    double calculateAverage();
    WaitTime(){
    totalWaitTime = 0;
    totalCustomers = 0;
    truetotal = 0;
    }
};

class Customer
{
public:
    unsigned int startTime;
    int weight;
    int destinationFlr;

    void randomWD(const ReadProp& prop);
};

class Elevator
{
public:
    int currentFloor;
    STATE elevator_state; //states if the elevator is idle, moving up, or moving down.
    BUSY busy;
    int totalWeight; //this is the totalWeight of all the customers currently on the elevator
    vector< queue<Customer> > destination;
    // A vector of queues, each index represents a destination floor
    // i.e [0] is first floor, [1] is second floor
    // there is a queue in each index that will store the people that want to get off at that floor
    vector<bool> upRequests;
    vector<bool> dnRequests;
    int timer; //this will increment slowly to 10 to indicate how many more cycles an elevator needs to stay on a floor for

    bool enterCustomer(Customer& customer, const ReadProp& prop);
    // returns false if cust can't enter elevator.
    //Othwerwise puts cust in the appropriate index, adjusts weight, and returns true
    bool exitCustomers(); // removes everyone that wants to get off on currFloor
    int numberOfRequests(int floorNumber, STATE upOrDown, STATE aboveOrBelow);
    bool goUp();
    bool goDown();
    void requestFromElevator(int destination);

    Elevator(const ReadProp& prop)
    {
        currentFloor = 1;          //elevator starts on first floor
        elevator_state = IDLE;
        busy = NO;
        totalWeight = 0;
        timer = 0;
        upRequests.resize(prop.floors);
        dnRequests.resize(prop.floors);
        destination.resize(prop.floors);
        for (int i = 0; i < prop.floors; i++){
            upRequests[i] = false;
            dnRequests[i] = false;
        }
    }
};

class Elevator_Bank
{

public:
    vector<Elevator> bank;

    void requestFromFloor(int origin, STATE desiredDirection, bool firstAttempt);
    int chooseBestElevator(int origin, STATE desiredDirection, bool firstAttempt);


    Elevator_Bank(const ReadProp& prop){

        bank.resize(prop.elevators, Elevator(prop));
    }

};

class Floor
{
public:
    vector< queue<Customer> > upQueue, dnQueue; //two vector of queues; each index represents a floor [0] = first floor, [1] = second floor
    //one vector will have a queue on each floor dedicated to going up
    //the other will have a queue dedicated to going down

    void generateCust(const ReadProp& prop, Elevator_Bank& bank, const unsigned int currentTime, WaitTime& wait);

    bool enterElevator(Elevator& elevator, const ReadProp& prop, Elevator_Bank& bank, WaitTime& wait, const unsigned int currentTime);

    Floor(const ReadProp& prop)
    {
        upQueue.resize(prop.floors);
        dnQueue.resize(prop.floors);
    }

};

class Simulator
{
protected:
    unsigned int simTime; //duration of the simulation in cycles. Each cycle represents 6 seconds.

public:
    void runSimulator(const ReadProp&);
    void cycleElevators(Elevator_Bank& bank, Floor& floor, const ReadProp& prop, WaitTime& wait, const unsigned int currentTime);
    Simulator(){
    simTime = 4800;
    }

};



#endif

