//File:         main.cpp
//Author(s):    Jonathan Zallas, Makina Celestine, Vishal Seecharran
//Created on:   October 30, 2011
//Description:  Program that simulates Elevators in a building with
//              user provided details.
//Usage:        Reads a *.prop file from command line in form
//              '<programname>.exe <filename>.prop'. The objective of
//              this simulation is to provide the number of
//              customers and the mean wait time for all the customers
//              generated in eight hours. If it is possible, the simulation
//              will also simulate the alternative with one less elevator
//              than what was provided in the *.prop file.
//Version:      1.0
//

#include <iostream>
#include "ElevatorSimulator.h"

using namespace std;

int main (int argc, const char * argv[])
{
    if(argc!=2){
        cout<<"Please follow the following format to start program: "<<argv[0]<<" <filename>"<<endl;
        return 0;
    }
    ReadProp prop(argv[1]); //creates an object called prop, and passes the filename(which is a c string) to the constructor
    srand(time(0));
    Simulator simulation;

    if((prop.elevators) >0){
    simulation.runSimulator(prop);
    }
    else
        return 0;
    if ((prop.elevators -1) >0){
    prop.elevators = prop.elevators-1;
    simulation.runSimulator(prop);
    }
    else
        return 0;  //Program terminates

}

