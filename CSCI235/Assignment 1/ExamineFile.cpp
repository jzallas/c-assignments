/******************************************************************************
Title :			ExamineFile.cpp
Author :		Jonathan Zallas
Created on :	September 8, 2011
Description :	Program that asks user for filename and outputs the length of
				shortest and longest words, the respective words, the line they
				are found on, and the average word length in the file.
Usage :			Type in the name of a *.txt file and hit return to get
				appropriate data
Version :		1.0
******************************************************************************/

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

class ExamineFile
{
public:
    string filename;
    void getOpenFilename()
    {
        cout << "Name of input file: ";
        getline(cin,filename);
        source.open(filename.c_str());
    };

    //Displays longest word, it's length, and the line it first appeared on.
    //Displays shortest word, it's length, and the line it first appeared on.
    //Displays the average word length.
    void calculateAndDisplay()
    {
        readUntilEnd();

        cout << "Longest word: " << longWord.size() << " characters \"" << longWord << "\" (line " << longLine << ")" << endl;
        if (shortWord.size() > 1)
        {
            cout << "Shortest word: " << shortWord.size() << " characters \"" << shortWord << "\" (line " << shortLine << ")" << endl;
        }
        else
        {
            cout << "Shortest word: " << shortWord.size() << " character \"" << shortWord << "\" (line " << shortLine << ")" << endl;
        }

        //Avoid divide by zero error in case file is empty
        if (totalChars != 0 && totalWords != 0)
        {
            cout << "Length of average word: " << totalChars/(float)totalWords << endl;
        }
        else
        {
            cout << "Length of average word: 0" << endl;
        }
    };

    ExamineFile()
    {
        longWord = "";
        shortWord = "";
        longLine = 1;
        shortLine = 1;
        totalChars = 0;
        totalWords = 0;
    };

private:
    ifstream source;
    string longWord;
    string shortWord;
    int longLine;
    int shortLine;
    int totalChars;
    int totalWords;

    //Takes a raw string, strip puncutation if it has any, and return the evaluated word.
    //Example: "Example" will return "Example"  while "E^x!a*mple!" will return "Example".
    string checkIfValid(string currentWord)
    {
        for(unsigned int i = 0; i < currentWord.size() ; i++)
        {
            if (!isalpha((char)currentWord.c_str()[i]))
            {
                if (currentWord.compare(i, 1, "\n"))
                {
                    currentWord.erase(i,1);
                    i--;
                }
            }
        }
        return (currentWord);
    };

    //Takes a string and adds its length to the total for the sake of calculating the average.
    void adjustTotal(string currentWord)
    {
        totalChars += currentWord.size();
        totalWords ++;
    };

    //Takes a string and the line that the string was found on and compares it's length to both
    //the shortest and longest word currently on record. If it's shorter, it updates the shortest
    //word and respectiveline; If it's longer, it updates the longest word and respective line.
    void compareLongShort(string currentWord, int currentLineNum)
    {
        //the first word that passes through here is always the shortest word until it is compared.
        if (shortWord.empty()) shortWord = currentWord;

        if (currentWord.size() < shortWord.size())
        {
            shortWord = currentWord;
            shortLine = currentLineNum;
        }
        if (currentWord.size() > longWord.size())
        {
            longWord = currentWord;
            longLine = currentLineNum;
        }


    };

    //Reads the file until the end. Determines whether the input is a word, newline, or end of file
    //and processes each accordingly.
    void readUntilEnd()
    {
        string currentWord = "";
        string currentLine;
        int currentLineNum = 0;

        while(getline(source, currentLine))
        {
            istringstream getWord(currentLine);
            currentLineNum++;
            while(getWord >> currentWord)
            {
                currentWord = checkIfValid(currentWord);
                adjustTotal(currentWord);
                compareLongShort(currentWord, currentLineNum);
            }
        }
    };


};
int main()
{
    ExamineFile test;
    test.getOpenFilename();
    test.calculateAndDisplay();
    return 0;
}
