//File:         Index.cpp
//Author(s):    Jonathan Zallas
//Created on:   November 24, 2011
//Description:  User interface. Handles all of the user commands.
//Usage:        Used together with Binary Search Tree and a file
//              parser. Interprets commands from user.
//Version:      1.0
//

#include "Index.h"

void Index::getUserCommand()
{
    cout << "Command: ";
    string userInput, command, word;
    userInput = command = word = "";
    istringstream getWord;
    getline(cin, userInput);
    getWord.str(userInput);
    getWord >> command >> word;
    if (command != "load")
        word = file.checkIfValid(word);
    if (command == "find")
        find(word);
    else if (command == "add")
        add(word);
    else if (command == "remove")
        remove(word);
    else if (command == "load")
        load(word);
    else if (command == "help")
        help();
    else if (command == "size")
        size();
    else if (command == "print")
        print(word);
    else if (command == "exit")
    {
        resetIndex();
        exit(0);
    }
    else
        cout << "Unknown command, type 'help' for a list of commands." << endl;
}

void Index::find(string& word)
{
    Node* node = index.retrieve(word);
    if (word.empty())
        cout << "Unknown command, type 'help' for a list of commands." << endl;
    else if (node!=NULL)
    {
        cout << "Word \"" << word << "\" appears on line:";
        for (unsigned int i = 0; i < node->lines.size(); i++)
            cout << " " << node->lines[i];
        cout << endl;
    }
    else
        cout << "\""<< word << "\" does not appear in this file." << endl;
}

void Index::add(string& word)
{
    if (word.empty())
        cout << "Unknown command, type 'help' for a list of commands." << endl;
    else
    {
        index.insert(word, 0);
        cout << "Word \"" << word << "\" added to index." << endl;
    }
}

void Index::remove(string& word)
{
    if (word.empty())
        cout << "Unknown command, type 'help' for a list of commands." << endl;
    else if(index.remove(word))
        cout << "Word \"" << word << "\" removed from index." << endl;
    else
        cout << "Word \"" << word << "\" could not be removed from index." << endl;
}

void Index::load(string& word)
{
    resetIndex();
    if (word.empty())
        cout << "Unknown command, type 'help' for a list of commands." << endl;
    else if (createIndex(word))
    {
        cout << "File \"" << word << "\" read & index generated." << endl;
    }
    else
        cout << "Error opening file." << endl;
}

void Index::help()
{
    cout << "The following commands are available:" << endl << endl;
    cout << "find [word] \t\tSearches the index for the typed word." << endl << endl;
    cout << "add [word] \t\tAdds the word to the index with a default of line 0." << endl << endl;
    cout << "remove [word] \t\tRemoves a word from the index if it exists." << endl << endl;
    cout << "load [filename] \tCreates new index using the words in the file; erases" << endl << "\t\t\tany pre-existing index." << endl << endl;
    cout << "size \t\t\tShows the number of words stored in the index." << endl << endl;
    cout << "print [IN/PRE/POST]\tIN : Prints the index using in-order traversal." << endl;
    cout << "\t\t\tPRE : Prints the index using pre-order traversal." << endl;
    cout << "\t\t\tPOST : Prints the index using post-order traversal." << endl << endl;
    cout << "exit \t\t\tExits the program." << endl << endl;
}

void Index::size()
{
    cout << "The index currently has " << index.count << " words." << endl;
}

void Index::print(string& word)
{
    for (unsigned int i = 0; i < word.size(); ++i) //convert to lowercase before continuing
        word[i] = tolower(word[i]);
    if (word == "in")
    {
        index.inOrderTraversal(index.root);
        cout << endl;
    }
    else if (word == "pre")
    {
        index.preOrderTraversal(index.root);
        cout << endl;
    }
    else if (word == "post")
    {
        index.postOrderTraversal(index.root);
        cout << endl;
    }
    else
        cout << "Unknown command, type 'help' for a list of commands." << endl;
}

bool Index::createIndex(string filename)
{
    if (file.openFile(filename))
    {
        while (!file.endOfFile)
        {
            if (file.getWord())
            {
                if (!file.currentWord.empty())
                    index.insert(file.currentWord, file.currentLineNum);
            }
        }
        file.closeFile();
        return true;
    }
    return false;
}

void Index::resetIndex()
{
    index.wipeTree();
}
