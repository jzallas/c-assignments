//File:         ReadFile.cpp
//Author(s):    Jonathan Zallas
//Created on:   November 24, 2011
//Description:  Reads and parses a file.
//Usage:        Performs all the tasks necessary when reading
//              and parsing a file.
//Version:      1.0
//

#include "ReadFile.h"

bool ReadFile::openFile(string userFile)
{
    filename = userFile;
    fileSource.open(filename.c_str());
    if (!fileSource.is_open())
    {
        fileSource.clear();
        return false;
    }
    endOfFile = false;
    currentLineNum = 0;
    return true;
}

void ReadFile::closeFile()
{
    if (fileSource.is_open())
        fileSource.close();
    else
        cout << "Error closing file." << endl;
}

bool ReadFile::getWord()
{
    if(!endOfFile)
    {
        if (lineSource >> currentWord) //Able to get a word from the stored line;
        {
            currentWord = checkIfValid(currentWord);
            return true;
        }
        else  //Unable to get a word from stored line;
        {
            lineSource.clear();
            if (getline(fileSource, currentLine))//unable to get a word but able to get a new line
            {
                lineSource.str(currentLine);
                currentLineNum++;
                return (getWord());
            }
            else //unable to get a word and unable to get a line
            {
                fileSource.clear();
                endOfFile = true;
            }
        }
    }
    return false;
}

string ReadFile::checkIfValid(string currentWord)
{
    for(unsigned int i = 0; i < currentWord.size() ; i++)
    {
        currentWord[i] = tolower(currentWord[i]);
        if (!isalpha((char)currentWord.c_str()[i]))
        {
            if (currentWord.compare(i, 1, "\n"))
            {
                currentWord.erase(i,1);
                i--;
            }
        }
    }
    return (currentWord);
}
