//File:         ReadFile.h
//Author(s):    Jonathan Zallas
//Created on:   November 24, 2011
//Description:  Reads and parses a file.
//Usage:        Performs all the tasks necessary when reading
//              and parsing a file.
//Version:      1.0
//

#ifndef READFILE_H
#define READFILE_H

#include <iostream>
#include <sstream>
#include <fstream>

using namespace std;

class ReadFile
{
public:
    ifstream fileSource;
    istringstream lineSource;
    string filename;
    bool endOfFile;
    string currentWord;
    string currentLine;
    unsigned int currentLineNum;

    string checkIfValid(string currentWord);
    bool getWord();
    bool openFile(string userFile);
    void closeFile();

    ReadFile()
    {
        endOfFile = false;
        currentLineNum = 0;
    }
};

#endif
