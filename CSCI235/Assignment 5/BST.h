//File:         BST.h
//Author(s):    Jonathan Zallas
//Created on:   November 24, 2011
//Description:  Doubly linked Binary Search Tree.
//Usage:        Each node in the tree stores words and the line numbers
//              that the word was found.
//Version:      1.0
//

#ifndef BST_H
#define BST_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

struct Node
{
    vector<unsigned int> lines;
    string word;
    Node *left;
    Node *right;
    Node *parent;
};

class BST
{
public:
    unsigned int count;
    Node* root;

    //looks for the node containing the word, returns pointer to the node if the word is found.
    //otherwise returns node that would be the parent if that word if it was in the tree.
    Node* seek(string word);

    //checks to see if the tree is empty
    bool isEmpty();

    //looks for the node containing the word, returns pointer to the node if the word is found.
    //otherwise returns NULL if not found.
    Node* retrieve(string word);

    //if the word did not already exist in the tree, creates a new node with the word and the line
    //it was found on and returns true; if the word already exists, it does not create a new node
    //but instead updates the node respective to that word with the line that it was found on.
    bool insert(string word, unsigned int line);

    //when a word is already in the tree, this looks through the vector that stores the lines
    //that this word appeared on. If the line is already recorded, it does nothing (it would be a dup entry).
    //If the line is not recorded, it adds the line and then sorts the vector from lowest to highest.
    void updateLineOccurrence(Node* node, unsigned int line);

    //removes a node from a tree
    bool remove(string word);

    //this finds the in order successor if a node has one, returns NULL if there is no successor
    Node* findInOrderSuccessor(Node* node);

    //this will extract the in order successor and reorganize any nodes that were related to the successor
    //if extraction was successful, it will return a pointer to the successor; if unsuccessful, will return NULL
    Node* extractInOrderSuccessor(Node* node);

    //empties tree
    void wipeTree();

    void inOrderTraversal(Node* node);
    void preOrderTraversal(Node* node);
    void postOrderTraversal(Node* node);

    BST() //constructor
    {
        root = NULL;
        count = 0;
    }
    ~BST() //destructor
    {
        wipeTree();
    }

};




#endif
