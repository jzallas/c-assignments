//File:         BST.cpp
//Author(s):    Jonathan Zallas
//Created on:   November 24, 2011
//Description:  Doubly linked Binary Search Tree.
//Usage:        Each node in the tree stores words and the line numbers
//              that the word was found.
//Version:      1.0
//

#include "BST.h"

bool BST::isEmpty()
{
    if (count == 0)
        return true;
    return false;
}

Node* BST::retrieve(string word)
{
    Node* currNode = seek(word);
    if (currNode != NULL) //first make sure the tree wasn't empty
    {
        if (currNode->word == word) //then check to see if seek returned the word or it's supposed parent
            return currNode;
    }
    return NULL;
}

Node* BST::seek(string word)
{
    Node* currNode = root;
    while (!isEmpty())
    {
        if (currNode->word == word) //if the current node has the word we're looking for, return pointer to that node
            return currNode;
        else if (word < currNode->word) //otherwise, if the word we're looking for comes before the currNode's word, there are two cases
        {
            if (currNode->left != NULL) //first case: if the next node is not null, continue onto the next node
            {
                currNode = currNode->left;
            }
            else //second case: if the next node is null, then the word does not exist yet, so return it's parent
            {
                return currNode;
            }
        }
        else if (word > currNode->word) //otherwise, if the word we're looking for comes after the currNode's word, there are two cases
        {
            if (currNode->right != NULL) //first case: if the next node is not null, continue onto the next node
            {
                currNode = currNode->right;
            }
            else //second case: if the next node is null, then the word does not exist yet, so return it's parent
            {
                return currNode;
            }
        }
    }
    //if the tree was empty, just return what the root is pointing to, which should be NULL
    return currNode;
}

void BST::updateLineOccurrence(Node* node, unsigned int line)
{
    bool alreadyExists = false;
    //check to see if the line that this word appears on is already documented
    for (unsigned int i = 0; i < node->lines.size(); i++)
    {
        if (node->lines[i] == line)
            alreadyExists = true;
    }
    //if the line that this word appears on was not previously recorded
    if (!alreadyExists)
    {
        node->lines.push_back(line); //add the line
        sort(node->lines.begin(), node->lines.end()); //sort the appeared lines from smallest to largest
    }
}

bool BST::insert(string word, unsigned int line)
{
    if(isEmpty()) //special case: if the tree is empty, create a new node and have the root point to that node
    {
        Node* newNode = new Node;
        newNode->word = word;
        newNode->lines.push_back(line);
        newNode->left = newNode->right = newNode->parent = NULL;
        root = newNode;
        count++;
        return true;
    }
    else
    {
        Node* currNode = seek(word);
        if (currNode->word == word) //first case: if the word exists already, just add update the node with the line it was found on
        {
            updateLineOccurrence(currNode, line);
            return true;
        }
        else //second case: if the word was not found, then the appropriate parent of the word was found. Create a new node and set it as the child of that parent.
        {
            if (word > currNode->word) //Child belongs on the right
            {
                Node* newNode = new Node;
                newNode->word = word;
                newNode->lines.push_back(line);
                newNode->left = newNode->right = newNode->parent = NULL;
                currNode->right = newNode;
                newNode->parent = currNode;
                count++;
                return true;
            }
            else if (word < currNode->word) //Child belongs on the left
            {
                Node* newNode = new Node;
                newNode->word = word;
                newNode->lines.push_back(line);
                newNode->left = newNode->right = newNode->parent = NULL;
                currNode->left = newNode;
                newNode->parent = currNode;
                count++;
                return true;
            }
        }
    }
    //if for some reason a node was never inserted, return false
    return false;
}

bool BST::remove(string word)
{
    Node* removeNode = retrieve(word);
    if (removeNode == NULL) //if the word is not found or the tree is empty, return false
        return false;
    if (removeNode == root) //Case 1: node to delete happens to be root
    {
        if (removeNode->left == NULL && removeNode->right == NULL) //Case 1A: This node is a leaf
        {
            root = NULL;
        }
        else if (removeNode->left != NULL && removeNode->right != NULL) //Case 1B: This node has two children
        {
            root = extractInOrderSuccessor(removeNode);

            //successor inherits all of the pointers related to the node marked for removal
            root->left = removeNode->left;
            root->right = removeNode->right;
            if (root->left != NULL)
                root->left->parent = root;
            if (root->right != NULL)
                root->right->parent = root;
        }
        else //Case 1C: This node has one child
        {
            if (removeNode->left != NULL) //the child is the left node
            {
                root = removeNode->left;
                removeNode->left->parent = NULL;
            }
            else if (removeNode->right != NULL) //the child is the right node
            {
                root = removeNode->right;
                removeNode->right->parent = NULL;
            }
        }
    }
    else //Case 2: node to delete is not the root
    {
        if (removeNode->left == NULL && removeNode->right == NULL) //Case 2A: This node is a leaf
        {
            if (removeNode->parent->left == removeNode) //the node to be removed is the left child of the parent
            {
                removeNode->parent->left = NULL;
            }
            else if (removeNode->parent->right == removeNode) //the node to be removed is the right child of the parent
            {
                removeNode->parent->right = NULL;
            }
        }
        else if (removeNode->left != NULL && removeNode->right != NULL) //Case 2B: This node has two children
        {
            Node* successor = extractInOrderSuccessor(removeNode);
            if (removeNode->parent->left == removeNode) //the node to be removed is the left child of the parent
            {
                removeNode->parent->left = successor;

                //successor inherits all of the pointers related to the node marked for removal
                successor->left = removeNode->left;
                successor->right = removeNode->right;
                if (successor->left != NULL)
                    successor->left->parent = successor;
                if (successor->right != NULL)
                    successor->right->parent = successor;
            }
            else if (removeNode->parent->right == removeNode) //the node to be removed is the right child of the parent
            {
                removeNode->parent->right = successor;

                //successor inherits all of the pointers related to the node marked for removal
                successor->left = removeNode->left;
                successor->right = removeNode->right;
                if (successor->left != NULL)
                    successor->left->parent = successor;
                if (successor->right != NULL)
                    successor->right->parent = successor;
            }
        }
        else //Case 2C: This node has one child
        {
            if (removeNode->left != NULL) //the child is the left node
            {
                if (removeNode->parent->left == removeNode) //the node to be removed is the left child of the parent
                {
                    removeNode->parent->left = removeNode->left;
                    removeNode->left->parent = removeNode->parent;
                }
                else if (removeNode->parent->right == removeNode) //the node to be removed is the right child of the parent
                {
                    removeNode->parent->right = removeNode->left;
                    removeNode->left->parent = removeNode->parent;
                }
            }
            else if (removeNode->right != NULL) //the child is the right node
            {
                if (removeNode->parent->left == removeNode) //the node to be removed is the left child of the parent
                {
                    removeNode->parent->left = removeNode->right;
                    removeNode->right->parent = removeNode->parent;
                }
                else if (removeNode->parent->right == removeNode) //the node to be removed is the right child of the parent
                {
                    removeNode->parent->right = removeNode->right;
                    removeNode->right->parent = removeNode->parent;
                }
            }
        }
    }
    delete removeNode;
    removeNode = NULL;
    count--;
    return true;
}

Node* BST::findInOrderSuccessor(Node* node)
{
    //in order successor is found by first going down to the right child (if there is no right child, an in order successor does not exist)
    //then there are two cases
    //first case: right child has left children, so you keep going down the left branches until you reach the last node; that is the successor
    //second case: right child has no left children; the right child is the successor
    Node* currNode = node;
    if (currNode->right == NULL) //does not have a successor
        return NULL;
    else if (currNode->right != NULL) //has a successor
    {
        currNode = currNode->right;
        if (currNode-> left != NULL) //first case
        {
            while (currNode->left != NULL)
            {
                currNode = currNode->left;
            }
            return currNode;
        }
    }
    return currNode; //defaults to second case

}

Node* BST::extractInOrderSuccessor(Node* node)
{
    Node* successor = findInOrderSuccessor(node);
    if (successor == NULL) //first case: this node doesnt have an in order successor (this should never happen anyway)
        return NULL;
    else if (successor->left == NULL && successor->right == NULL) //second case: successor is a leaf node
    {
        if (successor->parent->left == successor)//the successor is the left child of the parent
        {
            successor->parent->left = NULL;
            successor->parent = NULL;
            return successor;
        }
        else if (successor->parent->right == successor)//the successor is the right child of the parent
        {
            successor->parent->right = NULL;
            successor->parent = NULL;
            return successor;
        }
    }
    else if (successor->right != NULL) //third case: successor has a right child
    {
        if (successor->parent->left == successor)//the successor is the left child of the parent
        {
            //promote the successor's right child before extracting the successor
            successor->parent->left = successor->right;
            successor->right->parent = successor->parent;
            successor->parent = NULL;
            successor->right = NULL;
            return successor;

        }
        else if (successor->parent->right == successor)//the successor is the right child of the parent
        {
            //promote the successor's right child before extracting the successor
            successor->parent->right = successor->right;
            successor->right->parent = successor->parent;
            successor->parent = NULL;
            successor->right = NULL;
            return successor;
        }
    }
    return successor; //if extraction failed return NULL
}

void BST::wipeTree()
{
    while(!isEmpty())
    {
        remove(root->word);
    }
}

void BST::inOrderTraversal(Node* node)
{
    if(!isEmpty())
    {
        if (node->left != NULL)
            inOrderTraversal(node->left);
        cout << node->word<< " ";
        if (node->right !=NULL)
            inOrderTraversal(node->right);
    }
    else
        cout << "The index is empty.";
}

void BST::preOrderTraversal(Node* node)
{
    if (!isEmpty())
    {
        cout << node->word<< " ";
        if (node->left != NULL)
            preOrderTraversal(node->left);
        if (node->right != NULL)
            preOrderTraversal(node->right);
    }
    else
        cout << "The index is empty.";
}

void BST::postOrderTraversal(Node* node)
{
    if (!isEmpty())
    {
        if (node->left != NULL)
            postOrderTraversal(node->left);
        if (node->right != NULL)
            postOrderTraversal(node->right);
        cout << node->word<< " ";
    }
    else
        cout << "The index is empty.";
}

