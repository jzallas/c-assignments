//File:         main.cpp
//Author(s):    Jonathan Zallas
//Created on:   November 24, 2011
//Description:  Program that creates an interactive index of words in memory.
//Usage:        Waits for a command from the user. When running, type 'help'
//              for a list of commands and their respective descriptions.
//Version:      1.0
//

#include <iostream>
#include "Index.h"

using namespace std;

int main()
{
    Index newindex;
    cout << "Type 'help' for a list of commands." << endl;
    while(true)
    {
        newindex.getUserCommand();
    }
    return 0;
}
