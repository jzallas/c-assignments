//File:         Index.h
//Author(s):    Jonathan Zallas
//Created on:   November 24, 2011
//Description:  User interface. Handles all of the user commands.
//Usage:        Used together with Binary Search Tree and a file
//              parser. Interprets commands from user.
//Version:      1.0
//

#ifndef INDEX_H
#define INDEX_H

#include <iostream>
#include <cstdlib>
#include "Readfile.h"
#include "BST.h"

using namespace std;

class Index
{
public:
    ReadFile file;
    BST index;

    bool createIndex(string filename);
    void resetIndex();

    void getUserCommand();
    //user commands
    void find(string& word);
    void add(string& word);
    void remove(string& word);
    void load(string& word);
    void help();
    void print(string& word);
    void size();
};

#endif
