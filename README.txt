This just contains my assignments written in C++ from my relevant courses at Hunter College. Should you use any of the code, I recommend you either cite it properly or just do the work yourself because plagiarism is probably not the best way to become a good programmer. Each assignment contains the objective (if it was available electronically), the source code, and the grading comments (again if they were available electronically).

Also, here are the grades I received in each course respectively so you can judge how accurately I know these topics should you have any concerns as to how correct the answers are.

CODE		COURSE NAME				GRADE	TERM
CSCI235 	Algorithm Analysis and Design II	A+	Fall 2011
CSCI335 	Algorithm Analysis and Design III	A-	Fall 2012
CSCI340 	Operating Systems			A	Spring 2013